package cz.kybersquat.limeth.nnfill;

import com.beust.jcommander.ParameterException;
import com.google.common.collect.Lists;
import cz.kybersquat.limeth.nnfill.command.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Limeth
 */
public class NNFill
{
	private static final List<Class<? extends Subcommand>> subcommandArray = Lists.newArrayList(
			CreateWorldShapeSubcommand.class, CreateNetworkSubcommand.class, CreateSimulationSubcommand.class,
			CreateTrainingSubcommand.class, RunSubcommand.class, TrainSubcommand.class);

	private static final Map<String, Subcommand> subcommandMap = subcommandArray.stream().map(NNFill::instantiate).collect(Collectors.toMap(Subcommand::getName, c -> c));

	private static <T> T instantiate(Class<? extends T> clazz)
	{
		try
		{
			return clazz.newInstance();
		}
		catch(InstantiationException | IllegalAccessException e)
		{
			throw new RuntimeException(e);
		}
	}

	public static void main(String[] args)
	{
		CommandStorage storage = new CommandStorage();
		JCommanderFixed jc = new JCommanderFixed(storage);

		subcommandMap.values().forEach(jc::addCommand);
		jc.setProgramName("NNFill");
		jc.setAcceptUnknownOptions(true);

		try
		{
			jc.parseWithoutValidation(args);

			if(jc.getParsedCommand() == null)
			{
				jc.usage();
				return;
			}

			String subcommandName = jc.getParsedCommand();

			if(storage.isHelp())
			{
				jc.usage(subcommandName);
				return;
			}

			Subcommand subcommand = subcommandMap.get(subcommandName);

			jc.validateOptions();
			subcommand.execute(jc);
		}
		catch(ParameterException e)
		{
			System.out.println(e.getClass().getSimpleName() + ": " + e.getLocalizedMessage());
		}
	}
}
