package cz.kybersquat.limeth.nnfill.json;

import com.google.gson.*;

import java.lang.reflect.Type;

/**
 * @author Limeth
 */
public class JsonableAdapter implements JsonSerializer<Jsonable>, JsonDeserializer<Jsonable>
{
	private static final JsonableAdapter instance = new JsonableAdapter();
	private static final String PATH_CLASS = "type";
	private static final String PATH_DATA = "data";

	@Override
	public Jsonable deserialize(JsonElement jsonElement, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
	{
		JsonObject jsonObject = jsonElement.getAsJsonObject();
		String className = jsonObject.get(PATH_CLASS).getAsString();
		JsonElement data = jsonObject.get(PATH_DATA);

		try
		{
			Class<? extends Jsonable> clazz = (Class<? extends Jsonable>) Class.forName(className);

			return Jsonable.fromJson(clazz, data, context);
		}
		catch(ClassNotFoundException e)
		{
			throw new JsonParseException(e);
		}
	}

	@Override
	public JsonElement serialize(Jsonable src, Type typeOfSrc, JsonSerializationContext context)
	{
		JsonObject root = new JsonObject();

		root.addProperty(PATH_CLASS, src.getClass().getCanonicalName());
		root.add(PATH_DATA, src.toJson(context));

		return root;
	}

	public static JsonableAdapter getInstance()
	{
		return instance;
	}
}
