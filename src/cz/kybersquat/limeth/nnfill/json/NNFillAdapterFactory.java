package cz.kybersquat.limeth.nnfill.json;

import com.google.gson.GsonBuilder;
import neuroNet.limeth.json.NetworkAdapterFactory;

/**
 * @author Limeth
 */
public class NNFillAdapterFactory
{
	private NNFillAdapterFactory() {}

	public static GsonBuilder applySettings(GsonBuilder builder)
	{
		return NetworkAdapterFactory.applyGsonSettings(builder)
									.registerTypeHierarchyAdapter(Jsonable.class, JsonableAdapter.getInstance());
	}

	public static GsonBuilder createBuilder()
	{
		return applySettings(new GsonBuilder());
	}
}
