package cz.kybersquat.limeth.nnfill.json;

/**
 * @author Limeth
 */
public class JsonableDeserializationException extends RuntimeException
{
	public JsonableDeserializationException()
	{
	}

	public JsonableDeserializationException(String message)
	{
		super(message);
	}

	public JsonableDeserializationException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public JsonableDeserializationException(Throwable cause)
	{
		super(cause);
	}

	public JsonableDeserializationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
