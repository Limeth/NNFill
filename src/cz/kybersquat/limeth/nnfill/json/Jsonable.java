package cz.kybersquat.limeth.nnfill.json;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;

import java.lang.reflect.InvocationTargetException;

/**
 * Every Jsonable must contain the following constructor: {@code public Jsonable(JsonElement root, JsonDeserializationContext context)}
 * or the following method: {@code public static Jsonable fromJson(JsonElement root, JsonDeserializationContext context)}
 * @author Limeth
 */
public interface Jsonable
{
	static final String METHOD_DESERIALIZATION_NAME = "fromJson";

	static Jsonable fromJsonMethod(Class<? extends Jsonable> clazz, JsonElement root, JsonDeserializationContext context)
			throws NoSuchMethodException, InvocationTargetException, IllegalAccessException
	{
		return (Jsonable) clazz.getMethod(METHOD_DESERIALIZATION_NAME, JsonElement.class, JsonDeserializationContext.class)
		                       .invoke(null, root, context);
	}

	static Jsonable fromJsonConstructor(Class<? extends Jsonable> clazz, JsonElement root, JsonDeserializationContext context)
			throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException
	{
		return clazz.getConstructor(JsonElement.class, JsonDeserializationContext.class).newInstance(root, context);
	}

	static Jsonable fromJson(Class<? extends Jsonable> clazz, JsonElement root, JsonDeserializationContext context)
	{
		try
		{
			return fromJsonConstructor(clazz, root, context);
		}
		catch(NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e)
		{
			try
			{
				return fromJsonMethod(clazz, root, context);
			}
			catch(NoSuchMethodException | InvocationTargetException | IllegalAccessException e2)
			{
				throw new JsonableDeserializationException("Constructor Jsonable(JsonElement root, JsonDeserializationContext context)" +
				                                           " or method fromJson(JsonElement root, JsonDeserializationContext context) could" +
				                                           " not be accessed for class " + clazz.getCanonicalName() + ".");
			}
		}
	}

	JsonElement toJson(JsonSerializationContext context);
}
