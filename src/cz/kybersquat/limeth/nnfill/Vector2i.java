package cz.kybersquat.limeth.nnfill;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import cz.kybersquat.limeth.nnfill.json.Jsonable;

import java.util.Vector;

/**
 * @author Limeth
 */
public class Vector2i extends Vector<Integer> implements Cloneable, Jsonable
{
	private static final String JSON_FIELD_X = "x";
	private static final String JSON_FIELD_Y = "y";
	private static final int INDEX_X = 0;
	private static final int INDEX_Y = 1;

	public Vector2i(int x, int y)
	{
		super(2);
		setSize(2);
		setX(x);
		setY(y);
	}

	public Vector2i(Vector2i other)
	{
		this(other.getX(), other.getY());
	}

	public Vector2i()
	{
		this(0, 0);
	}

	@Override
	public synchronized Vector2i clone()
	{
		return new Vector2i(this);
	}

	public static Vector2i fromJson(JsonElement rawRoot, JsonDeserializationContext context)
	{
		JsonObject root = rawRoot.getAsJsonObject();
		int x = root.get(JSON_FIELD_X).getAsInt();
		int y = root.get(JSON_FIELD_Y).getAsInt();

		return new Vector2i(x, y);
	}

	@Override
	public JsonElement toJson(JsonSerializationContext context)
	{
		JsonObject root = new JsonObject();

		root.addProperty(JSON_FIELD_X, getX());
		root.addProperty(JSON_FIELD_Y, getY());

		return root;
	}

	public int distanceSquared(int x, int y)
	{
		int deltaX = x - getX();
		int deltaY = y - getY();

		return deltaX * deltaX + deltaY * deltaY;
	}

	public int distanceSquared(Vector<? extends Number> vector)
	{
		return distanceSquared(vector.get(INDEX_X).intValue(), vector.get(INDEX_Y).intValue());
	}

	public double distance(Vector<? extends Number> vector)
	{
		return Math.sqrt(distanceSquared(vector));
	}

	public Vector2i set(Vector<? extends Number> vector)
	{
		set(vector.get(INDEX_X).intValue(), vector.get(INDEX_Y).intValue());
		return this;
	}

	public Vector2i set(int x, int y)
	{
		setX(x);
		setY(y);
		return this;
	}

	public Vector2i add(Vector<? extends Number> vector)
	{
		add(vector.get(INDEX_X).intValue(), vector.get(INDEX_Y).intValue());
		return this;
	}

	public Vector2i add(int x, int y)
	{
		addX(x);
		addY(y);
		return this;
	}

	public int addX(int amount)
	{
		int newX = getX() + amount;

		setX(newX);

		return newX;
	}

	public int addY(int amount)
	{
		int newY = getY() + amount;

		setY(newY);

		return newY;
	}

	public int getX()
	{
		return get(INDEX_X);
	}

	public int getY()
	{
		return get(INDEX_Y);
	}

	public Vector2i setX(int x)
	{
		super.set(INDEX_X, x);
		return this;
	}

	public Vector2i setY(int y)
	{
		super.set(INDEX_Y, y);
		return this;
	}

	@Override
	public synchronized String toString()
	{
		return "Vector2i [" + getX() + "; " + getY() + "]";
	}
}
