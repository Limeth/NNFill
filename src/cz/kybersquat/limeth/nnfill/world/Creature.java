package cz.kybersquat.limeth.nnfill.world;

import cz.kybersquat.limeth.nnfill.Vector2i;
import cz.kybersquat.limeth.nnfill.world.shape.WorldShape;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

/**
 * @author Limeth
 */
public class Creature implements Entity
{
	private CreatureBrain brain;
	private Vector2i      origin;
	private World         world;
	private Vector2i      location;
	private       Face          face;
	private       int           step;

	public Creature(World world, Vector2i location, Face face, CreatureBrain brain)
	{
		this.world = world;
		this.location = location;
		this.origin = location;
		this.face = face;
		this.brain = brain;
	}

	public Creature attachToBrain()
	{
		brain.attachCreature(this);

		return this;
	}

	@Override
	public Paint getColor()
	{
		return Color.RED;
	}

	public Vector2i getOrigin()
	{
		return origin;
	}

	@Override
	public World getWorld()
	{
		return world;
	}

	@Override
	public void setWorld(World world)
	{
		this.world = world;
	}

	@Override
	public Vector2i getLocation()
	{
		return location;
	}

	@Override
	public void setLocation(Vector2i location)
	{
		this.location = location;
	}

	public void resetBrainOutputs()
	{
		brain.resetOutputs();
	}

	@Override
	public void update()
	{
		face = calculateDirectionFace();

		world.setOccupied(location, true);
		location.add(face.getVector());
		brain.resetOutputs();
		step++;
	}

	public Face calculateDirectionFace()
	{
		float directionFloat = brain.calculateDirection();
		Direction direction;

		if(directionFloat < 1 / 3F)
			direction = Direction.LEFT;
		else if(directionFloat > 2 / 3F)
			direction = Direction.RIGHT;
		else
			direction = Direction.FRONT;

		return face.getRelative(direction);
	}

	public boolean isAvailable(Direction direction)
	{
		Face directionFace = face.getRelative(direction);
		Vector2i directionVector = directionFace.getVector();
		Vector2i directionLocation = location.clone().add(directionVector);
		WorldShape shape = world.getShape();

		return !world.isOccupied(directionLocation) && shape.isInside(directionLocation);
	}

	public void setFace(Face face)
	{
		this.face = face;
	}

	public Face getFace()
	{
		return face;
	}

	public float getProgress()
	{
		return (float) step / world.getShape().getArea();
	}

	public void setStep(int step)
	{
		this.step = step;
	}

	public int getStep()
	{
		return step;
	}

	public CreatureBrain getBrain()
	{
		return brain;
	}

	public double getDistanceFromOrigin()
	{
		int deltaX = location.getX() - origin.getX();
		int deltaY = location.getY() - origin.getY();

		return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
	}
}
