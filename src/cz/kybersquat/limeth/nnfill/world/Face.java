package cz.kybersquat.limeth.nnfill.world;

import cz.kybersquat.limeth.nnfill.Vector2i;

import java.util.Random;

/**
 * @author Limeth
 */
public enum Face
{
	EAST(1, 0), NORTH(0, 1), WEST(-1, 0), SOUTH(0, -1);

	private final int x, y;

	private Face(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public Face getRelative(Direction direction)
	{
		int ordinal = ordinal();
		Face[] faces = values();
		int newOrdinal = (direction.getRelativeFaceIndex() + ordinal + faces.length) % faces.length;

		return faces[newOrdinal];
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	public Vector2i getVector()
	{
		return new Vector2i(x, y);
	}

	public static Face random()
	{
		Random random = new Random();
		Face[] values = values();

		return values[random.nextInt(values.length)];
	}
}
