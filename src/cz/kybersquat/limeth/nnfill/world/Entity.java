package cz.kybersquat.limeth.nnfill.world;

import cz.kybersquat.limeth.nnfill.Vector2i;
import javafx.scene.paint.Paint;

/**
 * @author Limeth
 */
public interface Entity
{
	World getWorld();
	void setWorld(World world);
	Vector2i getLocation();
	void setLocation(Vector2i location);
	Paint getColor();
	void update();
}
