package cz.kybersquat.limeth.nnfill.world.shape;

import cz.kybersquat.limeth.nnfill.Vector2i;
import cz.kybersquat.limeth.nnfill.json.Jsonable;
import javafx.scene.paint.Paint;

/**
 * @author Limeth
 */
public interface WorldShape extends Cloneable, Jsonable
{
	static int bruteforceArea(WorldShape shape)
	{
		int area = 0;

		for(int y = 0; y < shape.getHeight(); y++)
			for(int x = 0; x < shape.getWidth(); x++)
				if(shape.isInside(x, y))
					area++;

		return area;
	}

	int getWidth();
	int getHeight();
	int getArea();
	boolean isInside(Vector2i location);
	boolean isInside(int x, int y);
	WorldShape clone();
}
