package cz.kybersquat.limeth.nnfill.world.shape;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import cz.kybersquat.limeth.nnfill.Vector2i;

/**
 * @author Limeth
 */
public class TranslatedShape implements WorldShape
{
	private static final String JSON_FIELD_WORLDSHAPE = "shape";
	private static final String JSON_FIELD_LOCATION = "location";
	private WorldShape worldShape;
	private Vector2i location;

	public TranslatedShape(WorldShape worldShape, Vector2i location)
	{
		this.worldShape = worldShape;
		this.location = location;
	}

	public static TranslatedShape fromJson(JsonElement rawRoot, JsonDeserializationContext context)
	{
		JsonObject root = rawRoot.getAsJsonObject();
		WorldShape worldShape = context.deserialize(root.get(JSON_FIELD_WORLDSHAPE), WorldShape.class);
		Vector2i location = context.deserialize(root.get(JSON_FIELD_LOCATION), Vector2i.class);

		return new TranslatedShape(worldShape, location);
	}

	@Override
	public JsonElement toJson(JsonSerializationContext context)
	{
		JsonObject root = new JsonObject();

		root.add(JSON_FIELD_WORLDSHAPE, context.serialize(worldShape));
		root.add(JSON_FIELD_LOCATION, context.serialize(location));

		return root;
	}

	public int getAbsoluteWidth()
	{
		return getWidth() + getX();
	}

	public int getAbsoluteHeight()
	{
		return getHeight() + getY();
	}

	public int getX()
	{
		return location.getX();
	}

	public int getY()
	{
		return location.getY();
	}

	public Vector2i getLocation()
	{
		return location.clone();
	}

	public boolean isInsideAbsolute(int x, int y)
	{
		return isInside(x - getX(), y - getY());
	}

	public boolean isInsideAbsolute(Vector2i location)
	{
		return isInsideAbsolute(location.getX(), location.getY());
	}

	@Override
	public int getWidth()
	{
		return worldShape.getWidth();
	}

	@Override
	public int getHeight()
	{
		return worldShape.getHeight();
	}

	@Override
	public int getArea()
	{
		return worldShape.getArea();
	}

	@Override
	public boolean isInside(Vector2i location)
	{
		return worldShape.isInside(location);
	}

	@Override
	public boolean isInside(int x, int y)
	{
		return worldShape.isInside(x, y);
	}

	@Override
	public TranslatedShape clone()
	{
		return new TranslatedShape(worldShape.clone(), location.clone());
	}
}
