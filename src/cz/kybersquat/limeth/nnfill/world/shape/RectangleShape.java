package cz.kybersquat.limeth.nnfill.world.shape;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import cz.kybersquat.limeth.nnfill.Vector2i;

/**
 * @author Limeth
 */
public class RectangleShape extends AbstractWorldShape
{
	private static final String JSON_FIELD_WIDTH = "width";
	private static final String JSON_FIELD_HEIGHT = "height";

	public RectangleShape(int width, int height)
	{
		super(width, height);
	}

	public static RectangleShape fromJson(JsonElement rawJson, JsonDeserializationContext context)
	{
		JsonObject root = rawJson.getAsJsonObject();
		int width = root.get(JSON_FIELD_WIDTH).getAsInt();
		int height = root.get(JSON_FIELD_HEIGHT).getAsInt();

		return new RectangleShape(width, height);
	}

	@Override
	public JsonElement toJson(JsonSerializationContext context)
	{
		JsonObject root = new JsonObject();

		root.addProperty(JSON_FIELD_WIDTH, getWidth());
		root.addProperty(JSON_FIELD_HEIGHT, getHeight());

		return root;
	}

	@Override
	public int getArea()
	{
		return getWidth() * getHeight();
	}

	@Override
	public boolean isInside(Vector2i location)
	{
		return isInside(location.getX(), location.getY());
	}

	@Override
	public boolean isInside(int x, int y)
	{
		return x >= 0
		       && y >= 0
		       && x < getWidth()
		       && y < getHeight();
	}

	@Override
	public AbstractWorldShape clone()
	{
		return new RectangleShape(getWidth(), getHeight());
	}
}
