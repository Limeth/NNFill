package cz.kybersquat.limeth.nnfill.world.shape;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.gson.*;
import cz.kybersquat.limeth.nnfill.Vector2i;

import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Limeth
 */
public class UnitedShape implements WorldShape
{
	private final Set<TranslatedShape> shapes;
	private Integer lazyWidth;
	private Integer lazyHeight;
	private Integer lazyArea;

	public UnitedShape(Iterable<TranslatedShape> shapes)
	{
		this.shapes = Sets.newHashSet(shapes);
	}

	public UnitedShape(Iterator<TranslatedShape> shapes)
	{
		this.shapes = Sets.newHashSet(shapes);
	}

	public UnitedShape(TranslatedShape... shapes)
	{
		this.shapes = Sets.newHashSet(shapes);
	}

	public static UnitedShape fromJson(JsonElement rawRoot, JsonDeserializationContext context)
	{
		JsonArray array = rawRoot.getAsJsonArray();
		Set<TranslatedShape> shapes = Lists.newArrayList(array).stream()
		                                   .map(e -> context.<TranslatedShape>deserialize(e, TranslatedShape.class))
		                                   .collect(Collectors.toSet());

		return new UnitedShape(shapes);
	}

	@Override
	public JsonElement toJson(JsonSerializationContext context)
	{
		return context.serialize(shapes);
	}

	@Override
	public int getWidth()
	{
		if(lazyWidth != null)
			return lazyWidth;

		lazyWidth = 0;

		for(TranslatedShape shape : shapes)
		{
			int width = shape.getAbsoluteWidth();

			if(width > lazyWidth)
				lazyWidth = width;
		}

		return lazyWidth;
	}

	@Override
	public int getHeight()
	{
		if(lazyHeight != null)
			return lazyHeight;

		lazyHeight = 0;

		for(TranslatedShape shape : shapes)
		{
			int height = shape.getAbsoluteHeight();

			if(height > lazyHeight)
				lazyHeight = height;
		}

		return lazyHeight;
	}

	@Override
	public int getArea()
	{
		return lazyArea != null ? lazyArea : (lazyArea = WorldShape.bruteforceArea(this));
	}

	@Override
	public boolean isInside(Vector2i location)
	{
		return isInside(location.getX(), location.getY());
	}

	@Override
	public boolean isInside(int x, int y)
	{
		for(TranslatedShape shape : shapes)
			if(shape.isInsideAbsolute(x, y))
				return true;

		return false;
	}

	@Override
	public WorldShape clone()
	{
		Set<TranslatedShape> shapes = this.shapes.stream().map(TranslatedShape::clone).collect(Collectors.toSet());

		return new UnitedShape(shapes);
	}

	public static Builder builder()
	{
		return new Builder();
	}

	public static class Builder
	{
		private final Set<TranslatedShape> shapes = Sets.newHashSet();

		private Builder() {}

		public Builder rectPos(int x1, int y1, int x2, int y2)
		{
			int minX = Math.min(x1, x2);
			int minY = Math.min(y1, y2);
			int maxX = Math.max(x1, x2);
			int maxY = Math.max(y1, y2);
			int width = maxX - minX + 1;
			int height = maxY - minY + 1;

			return rectSize(minX, minY, width, height);
		}

		public Builder rectSize(int x, int y, int width, int height)
		{
			Vector2i location = new Vector2i(x, y);
			TranslatedShape shape = new TranslatedShape(new RectangleShape(width, height), location);

			shapes.add(shape);

			return this;
		}

		public Builder circle(int x, int y, float radius)
		{
			Vector2i location = new Vector2i(x, y);
			TranslatedShape shape = new TranslatedShape(new CircleShape(radius), location);

			shapes.add(shape);

			return this;
		}

		public UnitedShape build()
		{
			return new UnitedShape(shapes);
		}
	}
}
