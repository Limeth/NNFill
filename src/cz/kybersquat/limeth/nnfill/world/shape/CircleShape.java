package cz.kybersquat.limeth.nnfill.world.shape;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import cz.kybersquat.limeth.nnfill.Vector2i;

/**
 * @author Limeth
 */
public class CircleShape implements WorldShape
{
	private static final String JSON_FIELD_RADIUS = "radius";
	private float radius;
	private Integer lazyArea;

	public CircleShape(float radius)
	{
		this.radius = radius;
	}

	public static CircleShape fromJson(JsonElement rawRoot, JsonDeserializationContext context)
	{
		JsonObject root = rawRoot.getAsJsonObject();
		float radius = root.get(JSON_FIELD_RADIUS).getAsFloat();

		return new CircleShape(radius);
	}

	@Override
	public JsonElement toJson(JsonSerializationContext context)
	{
		JsonObject root = new JsonObject();

		root.addProperty(JSON_FIELD_RADIUS, radius);

		return root;
	}

	@Override
	public int getWidth()
	{
		return 1 + (int) getDiameter();
	}

	@Override
	public int getHeight()
	{
		return 1 + (int) getDiameter();
	}

	public float getDiameter()
	{
		return radius * 2;
	}

	@Override
	public int getArea()
	{
		return lazyArea != null ? lazyArea : (lazyArea = WorldShape.bruteforceArea(this));
	}

	@Override
	public boolean isInside(int x, int y)
	{
		float centerX = getWidth() / 2F;
		float centerY = getHeight() / 2F;
		float radiusSquared = radius * radius;
		float relX = x - centerX;
		float relY = y - centerY;

		return relX * relX + relY * relY <= radiusSquared;
	}

	@Override
	public boolean isInside(Vector2i location)
	{
		return isInside(location.getX(), location.getY());
	}

	@Override
	public CircleShape clone()
	{
		CircleShape shape = new CircleShape(radius);

		shape.lazyArea = lazyArea;

		return shape;
	}
}
