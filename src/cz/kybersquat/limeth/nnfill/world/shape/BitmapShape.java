package cz.kybersquat.limeth.nnfill.world.shape;

import com.beust.jcommander.internal.Lists;
import com.google.common.base.Preconditions;
import com.google.gson.*;
import cz.kybersquat.limeth.nnfill.Vector2i;

import java.util.BitSet;
import java.util.Collections;
import java.util.List;

/**
 * @author Limeth
 */
public class BitmapShape implements WorldShape
{
	public static final char CHARACTER_SPACE = '.';
	public static final char CHARACTER_WALL = '/';
	private final BitSet bitset;
	private final int    width;
	private final int    height;

	public BitmapShape(BitSet bitset, int width, int height)
	{
		Preconditions.checkNotNull(bitset, "The BitSet must not be null!");
		Preconditions.checkArgument(bitset.size() >= width * height, "The size is invalid.");

		this.bitset = bitset;
		this.width = width;
		this.height = height;
	}

	public BitmapShape(int width, int height)
	{
		this(new BitSet(width * height), width, height);
	}

	public static BitmapShape fromJson(JsonElement rawRoot, JsonDeserializationContext context)
	{
		int maxWidth = 0;
		int height = 0;
		List<BitSet> rows = Lists.newArrayList();
		JsonArray root = rawRoot.getAsJsonArray();

		for(JsonElement rawRow : root)
		{
			String stringRow = rawRow.getAsString();
			char[] charRow = stringRow.toCharArray();
			int width = charRow.length;
			BitSet row = new BitSet(width);

			for(int x = 0; x < width; x++)
				if(charRow[x] == CHARACTER_SPACE)
					row.set(x);

			rows.add(row);

			if(width > maxWidth)
				maxWidth = width;

			height++;
		}

		Collections.reverse(rows);

		BitSet bitset = new BitSet(maxWidth * height);

		for(int y = 0; y < rows.size(); y++)
		{
			BitSet row = rows.get(y);

			for(int x = 0; x < row.size(); x++)
				if(row.get(x))
					bitset.set(x + y * maxWidth);
		}

		return new BitmapShape(bitset, maxWidth, height);
	}

	@Override
	public JsonElement toJson(JsonSerializationContext context)
	{
		JsonArray root = new JsonArray();

		for(int y = height - 1; y >= 0; y--)
		{
			StringBuilder rowBuilder = new StringBuilder();

			for(int x = 0; x < width; x++)
				rowBuilder.append(bitset.get(toIndex(x, y)) ? CHARACTER_SPACE : CHARACTER_WALL);

			root.add(new JsonPrimitive(rowBuilder.toString()));
		}

		return root;
	}

	@Override
	public int getWidth()
	{
		return width;
	}

	@Override
	public int getHeight()
	{
		return height;
	}

	@Override
	public int getArea()
	{
		return WorldShape.bruteforceArea(this);
	}

	@Override
	public boolean isInside(Vector2i location)
	{
		return isInside(location.getX(), location.getY());
	}

	@Override
	public boolean isInside(int x, int y)
	{
		if(x < 0 || y < 0 || x >= width || y >= height)
			return false;

		return bitset.get(toIndex(x, y));
	}

	private int toIndex(int x, int y)
	{
		return x + y * width;
	}

	@Override
	public WorldShape clone()
	{
		return new BitmapShape((BitSet) bitset.clone(), width, height);
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(o == null || getClass() != o.getClass())
			return false;

		BitmapShape that = (BitmapShape) o;

		if(width != that.width)
			return false;
		if(height != that.height)
			return false;
		if(!bitset.equals(that.bitset))
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = bitset.hashCode();
		result = 31 * result + width;
		result = 31 * result + height;
		return result;
	}
}
