package cz.kybersquat.limeth.nnfill.world.shape;

/**
 * @author Limeth
 */
public abstract class AbstractWorldShape implements WorldShape
{
	private int width;
	private int height;

	public AbstractWorldShape(int width, int height)
	{
		this.width = width;
		this.height = height;
	}

	public abstract AbstractWorldShape clone();

	@Override
	public int getWidth()
	{
		return width;
	}

	@Override
	public int getHeight()
	{
		return height;
	}
}
