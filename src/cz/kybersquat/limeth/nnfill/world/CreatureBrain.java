package cz.kybersquat.limeth.nnfill.world;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import cz.kybersquat.limeth.nnfill.json.Jsonable;
import neuroNet.limeth.network.NeuralLayer;
import neuroNet.limeth.network.NeuralNetwork;
import neuroNet.limeth.network.functions.ActivationFunction;
import neuroNet.limeth.network.neurons.InputNeuron;
import neuroNet.limeth.network.neurons.OutputNeuron;

/**
 * @author Limeth
 */
public class CreatureBrain implements Cloneable, Jsonable
{
	private static final int NEURON_AMOUNT_INPUT = 5;
	private static final int NEURON_AMOUNT_OUTPUT = 1;
	private static final int NEURON_INDEX_INPUT_DISTANCE = 0;
	private static final int NEURON_INDEX_INPUT_PROGRESS = 1;
	private static final int NEURON_INDEX_INPUT_LEFT = 2;
	private static final int NEURON_INDEX_INPUT_RIGHT = 3;
	private static final int NEURON_INDEX_INPUT_FRONT = 4;
	private static final int NEURON_INDEX_OUTPUT_DIRECTION = 0;
	private final NeuralNetwork network;
	private Creature creature;

	private CreatureBrain(NeuralNetwork network)
	{
		this.network = network.clone();
	}

	public static CreatureBrain of(NeuralNetwork network)
	{
		try
		{
			validate(network);
		}
		catch(InvalidBrainException e)
		{
			throw new RuntimeException(e);
		}

		return new CreatureBrain(network);
	}

	public static void validate(NeuralNetwork network) throws InvalidBrainException
	{
		try
		{
			NeuralLayer firstLayer = network.getFirst();

			if(firstLayer.size() < NEURON_AMOUNT_INPUT)
				throw new InvalidBrainException("The network must have at least " + NEURON_AMOUNT_INPUT + " input neurons!");

			for(int i = 0; i < NEURON_AMOUNT_INPUT; i++)
				if(!(firstLayer.get(i) instanceof InputNeuron))
					throw new InvalidBrainException("The network must have at least " + NEURON_AMOUNT_INPUT + " input neurons!");

			NeuralLayer lastLayer = network.getLast();

			if(lastLayer.size() < NEURON_AMOUNT_OUTPUT)
				throw new InvalidBrainException("The network must have at least " + NEURON_AMOUNT_OUTPUT + " output neurons!");

			for(int i = 0; i < NEURON_AMOUNT_OUTPUT; i++)
				if(!(lastLayer.get(i) instanceof OutputNeuron))
					throw new InvalidBrainException("The network must have at least " + NEURON_AMOUNT_OUTPUT + " output neurons!");
		}
		catch(Exception e)
		{
			throw new InvalidBrainException(e);
		}
	}

	public static CreatureBrain fromJson(JsonElement root, JsonDeserializationContext context)
	{
		return of(context.deserialize(root, NeuralNetwork.class));
	}

	@Override
	public JsonElement toJson(JsonSerializationContext context)
	{
		return context.serialize(network);
	}

	public static NeuralNetwork createNetwork(ActivationFunction activationFunction, float weightRange, int[] hiddenLayers, boolean bias)
	{
		NeuralNetwork network = new NeuralNetwork(activationFunction, weightRange);

		NeuralLayer.createSimpleInputLayer(network, null, NEURON_AMOUNT_INPUT, bias);

		for(int hiddenLayer : hiddenLayers)
			NeuralLayer.createSimpleHiddenLayer(network, null, hiddenLayer, bias);

		NeuralLayer.createSimpleOutputLayer(network, null, NEURON_AMOUNT_OUTPUT);

		return network;
	}

	public void attachCreature(Creature creature)
	{
		NeuralLayer firstLayer = network.getFirst();

		((InputNeuron) firstLayer.get(NEURON_INDEX_INPUT_DISTANCE)).setSupplier(() -> (float) creature.getDistanceFromOrigin());
		((InputNeuron) firstLayer.get(NEURON_INDEX_INPUT_PROGRESS)).setSupplier(creature::getProgress);
		((InputNeuron) firstLayer.get(NEURON_INDEX_INPUT_LEFT)).setSupplier(() -> creature.isAvailable(Direction.LEFT) ? 1F : 0F);
		((InputNeuron) firstLayer.get(NEURON_INDEX_INPUT_RIGHT)).setSupplier(() -> creature.isAvailable(Direction.RIGHT) ? 1F : 0F);
		((InputNeuron) firstLayer.get(NEURON_INDEX_INPUT_FRONT)).setSupplier(() -> creature.isAvailable(Direction.FRONT) ? 1F : 0F);

		this.creature = creature;
	}

	public Creature detachCreature()
	{
		Creature previous = creature;
		creature = null;
		int i = 0;

		network.getFirst()
		       .stream()
		       .limit(NEURON_AMOUNT_INPUT)
		       .map(InputNeuron.class::cast)
		       .forEach(InputNeuron::removeSupplier);

		return previous;
	}

	public Creature getAttachedCreature()
	{
		return creature;
	}

	public boolean isCreatureAttached()
	{
		return creature != null;
	}

	public void resetOutputs()
	{
		network.resetOutputs();
	}

	public float calculateDirection()
	{
		return getOutputDirection().getOutput();
	}

	private OutputNeuron getOutputDirection()
	{
		return (OutputNeuron) network.getLast().get(NEURON_INDEX_OUTPUT_DIRECTION);
	}

	@Override
	public CreatureBrain clone()
	{
		return new CreatureBrain(network.clone());
	}

	public NeuralNetwork getNetwork()
	{
		return network;
	}

	public static class InvalidBrainException extends Exception
	{
		private InvalidBrainException() {
			super();
		}

		private InvalidBrainException(String message) {
			super(message);
		}

		private InvalidBrainException(String message, Throwable cause) {
			super(message, cause);
		}

		private InvalidBrainException(Throwable cause) {
			super(cause);
		}
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(o == null || getClass() != o.getClass())
			return false;

		CreatureBrain that = (CreatureBrain) o;

		if(!network.equals(that.network))
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		return network.hashCode();
	}
}
