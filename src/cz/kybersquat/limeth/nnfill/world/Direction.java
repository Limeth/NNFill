package cz.kybersquat.limeth.nnfill.world;

/**
 * @author Limeth
 */
public enum Direction
{
	LEFT(-1), RIGHT(0), FRONT(1);

	private int relativeFaceIndex;

	Direction(int relativeFaceIndex)
	{
		this.relativeFaceIndex = relativeFaceIndex;
	}

	public int getRelativeFaceIndex()
	{
		return relativeFaceIndex;
	}
}
