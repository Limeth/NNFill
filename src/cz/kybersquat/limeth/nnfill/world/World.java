package cz.kybersquat.limeth.nnfill.world;

import cz.kybersquat.limeth.nnfill.Vector2i;
import cz.kybersquat.limeth.nnfill.world.shape.WorldShape;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

/**
 * @author Limeth
 */
public class World
{
	private final WorldShape   shape;
	private final List<Entity> entities;
	private final BitSet       surface;

	public World(WorldShape shape, List<Entity> entities)
	{
		this.shape = shape;
		this.entities = new ArrayList<>(entities);
		surface = new BitSet(shape.getWidth() * shape.getHeight());
	}

	public World(WorldShape shape)
	{
		this.shape = shape;
		this.entities = new ArrayList<>();
		surface = new BitSet(shape.getWidth() * shape.getHeight());
	}

	public void clear()
	{
		entities.clear();
		surface.clear();
	}

	public void update()
	{
		entities.forEach(Entity::update);
	}

	public String getDebug()
	{
		StringBuilder info = new StringBuilder();

		for(int y = shape.getHeight() - 1; y >= 0; y--)
		{
			for(int x = 0; x < shape.getWidth(); x++)
			{
				info.append(isOccupied(x, y) ? "#" : (shape.isInside(x, y) ? "." : "-"));
			}

			info.append('\n');
		}

		return info.toString();
	}

	public WorldShape getShape()
	{
		return shape;
	}

	public boolean isOccupied(int x, int y)
	{
		int index = getSurfaceIndex(x, y);

		if(index < 0 || index >= surface.size())
			return false;

		return surface.get(index);
	}

	public boolean isOccupied(Vector2i location)
	{
		return isOccupied(location.getX(), location.getY());
	}

	public void setOccupied(int x, int y, boolean value)
	{
		int index = getSurfaceIndex(x, y);

		if(index < 0 || index >= surface.size())
			return;

		surface.set(index, value);
	}

	public void setOccupied(Vector2i location, boolean value)
	{
		setOccupied(location.getX(), location.getY(), value);
	}

	private int getSurfaceIndex(int x, int y)
	{
		return x + y * shape.getWidth();
	}

	public BitSet getSurface()
	{
		return surface;
	}

	public List<Entity> getEntities()
	{
		return entities;
	}
}
