package cz.kybersquat.limeth.nnfill.command;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import cz.kybersquat.limeth.nnfill.command.validators.ExistingDirectory;
import cz.kybersquat.limeth.nnfill.command.validators.ExistingFile;
import cz.kybersquat.limeth.nnfill.command.validators.MissingDirectory;
import cz.kybersquat.limeth.nnfill.simulation.training.AbstractTraining;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * @author Limeth
 */
@Parameters(commandNames = "train", commandDescription = "Begins a Training from a file")
public class TrainSubcommand implements Subcommand
{
	@Parameter(names = {"-t", "--training"}, required = true, validateValueWith = ExistingFile.class, description = "The Training file, will read from the default input stream if not specified")
	private File trainingFile;

	@Parameter(names = {"-d", "--directory"}, validateValueWith = ExistingDirectory.class, description = "The directory where the trained networks will be saved")
	private File outputDirectory;

	@Parameter(names = {"-o", "--output"}, validateValueWith = MissingDirectory.class,
			description = "The output file. If this argument is omitted, the output goes to the console")
	private File outputFile;

	@Override
	public void execute(JCommander jCommander) throws ParameterException
	{
		try
		{
			executeUnsafe(jCommander);
		}
		catch(FileNotFoundException e)
		{
			throw new RuntimeException(e);
		}
	}

	private void executeUnsafe(JCommander jCommander) throws FileNotFoundException
	{
		AbstractTraining training = SubcommandUtil.load(AbstractTraining.class, trainingFile);
		SubcommandUtil.TUIUpgradeHandler upgradeHandler = new SubcommandUtil.TUIUpgradeHandler(outputFile, outputDirectory);

		training.setUpgradeHandler(upgradeHandler);
		training.begin();
		upgradeHandler.close();
	}
}
