package cz.kybersquat.limeth.nnfill.command;

import com.beust.jcommander.Parameter;

/**
 * @author Limeth
 */
public class CommandStorage
{
	@Parameter(names = {"-h", "--help"}, help = true, description = "Display help")
	private boolean help;

	public boolean isHelp()
	{
		return help;
	}
}
