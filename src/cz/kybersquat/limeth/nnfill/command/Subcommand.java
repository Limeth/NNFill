package cz.kybersquat.limeth.nnfill.command;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

/**
 * @author Limeth
 */
public interface Subcommand
{
	static String getName(Subcommand subcommand)
	{
		return subcommand.getClass().getAnnotation(Parameters.class).commandNames()[0];
	}

	void execute(JCommander jCommander) throws ParameterException;
}
