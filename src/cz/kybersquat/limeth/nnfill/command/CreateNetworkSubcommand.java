package cz.kybersquat.limeth.nnfill.command;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import cz.kybersquat.limeth.nnfill.command.validators.MissingDirectory;
import cz.kybersquat.limeth.nnfill.world.CreatureBrain;
import neuroNet.limeth.network.NeuralNetwork;
import neuroNet.limeth.network.functions.ActivationFunction;

import java.io.File;
import java.util.List;

/**
 * @author Limeth
 */
@Parameters(commandNames = {"createNetwork"}, commandDescription = "Creates a neural network")
public class CreateNetworkSubcommand implements Subcommand
{
	@Parameter(names = {"-l", "--layers"}, required = true, variableArity = true, description = "Specify the amount of neurons in each hidden layer")
	private List<Integer> layers;

	@Parameter(names = {"-a", "--activation-function"}, description = "Specify the network activation function")
	private ActivationFunction activationFunction = ActivationFunction.LINEAR;

	@Parameter(names = {"-b", "--no-bias"}, description = "Do not add bias neurons to the layers")
	private boolean omitBias;

	@Parameter(names = {"-w", "--weight-range"}, description = "The range of random weight for each connection: <-w; w)")
	private float weightRange = 0;

	@Parameter(names = {"-o", "--output"}, validateValueWith = MissingDirectory.class, description = "The output file. If this argument is omitted, the output goes to the console")
	private File outputFile;

	public List<Integer> getLayers()
	{
		return layers;
	}

	public ActivationFunction getActivationFunction()
	{
		return activationFunction;
	}

	public boolean isBias()
	{
		return !omitBias;
	}

	public boolean isOmitBias()
	{
		return omitBias;
	}

	public File getOutputFile()
	{
		return outputFile;
	}

	@Override
	public void execute(JCommander jCommander)
	{
		int[] layerArray = new int[layers.size()];

		for(int i = 0; i < layers.size(); i++)
		{
			int neurons = layers.get(i);

			if(neurons <= 0)
				throw new ParameterException("The amount of neurons per layer must be larger than 0!");

			layerArray[i] = neurons;
		}

		NeuralNetwork network = CreatureBrain.createNetwork(activationFunction, weightRange, layerArray, !omitBias);

		SubcommandUtil.save(network, outputFile);
	}
}
