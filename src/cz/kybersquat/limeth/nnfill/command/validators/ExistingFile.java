package cz.kybersquat.limeth.nnfill.command.validators;

import com.beust.jcommander.IValueValidator;
import com.beust.jcommander.ParameterException;

import java.io.File;

/**
 * @author Limeth
 */
public class ExistingFile implements IValueValidator<File>
{
	@Override
	public void validate(String name, File value) throws ParameterException
	{
		if(!value.isFile())
			throw new ParameterException("The file '" + value + "' of parameter '" + name + "' does not exist!");
	}
}
