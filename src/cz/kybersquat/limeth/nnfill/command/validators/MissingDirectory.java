package cz.kybersquat.limeth.nnfill.command.validators;

import com.beust.jcommander.IValueValidator;
import com.beust.jcommander.ParameterException;

import java.io.File;

/**
 * @author Limeth
 */
public class MissingDirectory implements IValueValidator<File>
{
	@Override
	public void validate(String name, File value) throws ParameterException
	{
		if(value.isDirectory())
			throw new ParameterException("The path '" + value + "' of parameter '" + name + "' leads to a directory.");
	}
}
