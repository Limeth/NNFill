package cz.kybersquat.limeth.nnfill.command.converters;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.internal.Maps;
import com.google.common.base.Joiner;
import cz.kybersquat.limeth.nnfill.simulation.FilterLayer;
import cz.kybersquat.limeth.nnfill.simulation.training.layers.ExpansionFilterLayer;
import cz.kybersquat.limeth.nnfill.simulation.training.layers.LimitationFilterLayer;
import cz.kybersquat.limeth.nnfill.simulation.training.layers.MutationFilterLayer;
import cz.kybersquat.limeth.nnfill.simulation.training.layers.ReductionFilterLayer;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * @author Limeth
 */
public class FilterLayerConverter implements IStringConverter<FilterLayer>
{
	static
	{
		Map<String, Class<? extends FilterLayer>> map = Maps.newHashMap();

		map.put("expansion", ExpansionFilterLayer.class);
		map.put("limitation", LimitationFilterLayer.class);
		map.put("mutation", MutationFilterLayer.class);
		map.put("reduction", ReductionFilterLayer.class);

		classMap = map;
	}

	private static final Map<String, Class<? extends FilterLayer>> classMap;
	private static final String METHOD_NAME_PARSE  = "parse";
	private static final String METHOD_NAME_SYNTAX = "getSyntax";

	@Override
	public FilterLayer convert(String value)
	{
		String[] args = value.split("[,:;]");
		Class<? extends FilterLayer> clazz = classMap.get(args[0].toLowerCase());

		if(clazz == null)
			throw new ParameterException("Available FilterLayers: " + Joiner.on(", ").join(classMap.keySet()));

		String[] cutArgs = new String[args.length - 1];

		System.arraycopy(args, 1, cutArgs, 0, cutArgs.length);

		try
		{
			return parse(args[0].toLowerCase(), clazz, cutArgs);
		}
		catch(FilterLayerParseException e)
		{
			throw new ParameterException(e);
		}
	}

	public static <T extends FilterLayer> T parse(String name, Class<T> clazz, String[] args) throws FilterLayerParseException
	{
		try
		{
			return (T) clazz.getMethod(METHOD_NAME_PARSE, String[].class).invoke(null, (Object) args);
		}
		catch(InvocationTargetException e)
		{
			try
			{
				Method syntaxMethod = clazz.getMethod(METHOD_NAME_SYNTAX);

				if(syntaxMethod == null)
					throw new FilterLayerParseException("Could not parse the filter layer, syntax not specified.", e);
				else
				{
					String[] syntax = (String[]) syntaxMethod.invoke(null);
					StringBuilder builder = new StringBuilder(name)
							.append(':');

					Joiner.on(':').appendTo(builder, syntax);

					throw new FilterLayerParseException("Could not parse the filter layer, syntax: " + builder, e);
				}
			}
			catch(IllegalAccessException | InvocationTargetException | NoSuchMethodException e2)
			{
				throw new FilterLayerParseException("Invalid " + METHOD_NAME_SYNTAX + " method in class " + clazz.getCanonicalName(), e2);
			}
		}
		catch(IllegalAccessException | NoSuchMethodException | ClassCastException e)
		{
			throw new FilterLayerParseException("Invalid " + METHOD_NAME_PARSE + " method in class " + clazz.getCanonicalName(), e);
		}
	}

	public static class FilterLayerParseException extends RuntimeException
	{
		public FilterLayerParseException()
		{
		}

		public FilterLayerParseException(String message)
		{
			super(message);
		}

		public FilterLayerParseException(String message, Throwable cause)
		{
			super(message, cause);
		}

		public FilterLayerParseException(Throwable cause)
		{
			super(cause);
		}

		public FilterLayerParseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
		{
			super(message, cause, enableSuppression, writableStackTrace);
		}
	}
}
