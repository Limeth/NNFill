package cz.kybersquat.limeth.nnfill.command.converters;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.ParameterException;
import com.google.common.collect.Lists;
import cz.kybersquat.limeth.nnfill.Vector2i;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Limeth
 */
public class Vector2iConverter implements IStringConverter<Vector2i>
{
	private static final Pattern PATTERN_VALUES = Pattern.compile("[\\+-]?\\d+");

	@Override
	public Vector2i convert(String value)
	{
		List<Integer> list = Lists.newArrayList();
		Matcher matcher = PATTERN_VALUES.matcher(value);

		while(matcher.find())
			list.add(Integer.parseInt(matcher.group(0)));

		if(list.size() != 2)
			throw new ParameterException("The Vector2i has too many values, 2 required.");

		int x, y;

		try
		{
			x = list.get(0);
			y = list.get(1);
		}
		catch(IndexOutOfBoundsException e)
		{
			throw new ParameterException("Not enough values for Vector2i specified.");
		}

		return new Vector2i(x, y);
	}
}
