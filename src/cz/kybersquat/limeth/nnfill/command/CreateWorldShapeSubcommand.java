package cz.kybersquat.limeth.nnfill.command;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import cz.kybersquat.limeth.nnfill.command.validators.MissingDirectory;
import cz.kybersquat.limeth.nnfill.world.shape.BitmapShape;

import java.io.File;
import java.util.BitSet;

/**
 * @author Limeth
 */
@Parameters(commandNames = "createWorldShape", commandDescription = "Create a BitmapShape file")
public class CreateWorldShapeSubcommand implements Subcommand
{
	@Parameter(names = {"-w", "--width"}, required = true, description = "The width of the world")
	private int width;

	@Parameter(names = {"-h", "--height"}, required = true, description = "The height of the world")
	private int height;

	@Parameter(names = {"-d", "-v", "--default", "--value", "--default-value"}, arity = 1, description = "The value used to fill the world. True for space, false for walls")
	private boolean defaultValue = false;

	@Parameter(names = {"-o", "--output"}, validateValueWith = MissingDirectory.class, description = "The output file. If this argument is omitted, the output goes to the console")
	private File outputFile;

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public boolean isDefaultValue()
	{
		return defaultValue;
	}

	public File getOutputFile()
	{
		return outputFile;
	}

	@Override
	public void execute(JCommander jCommander)
	{
		if(width <= 0)
			throw new ParameterException("The width must be larger than 0!");
		if(height <= 0)
			throw new ParameterException("The height must be larger than 0!");

		BitSet bitset = new BitSet(width * height);

		if(defaultValue)
			bitset.set(0, width * height, true);

		BitmapShape shape = new BitmapShape(bitset, width, height);

		SubcommandUtil.save(shape, outputFile);
	}
}
