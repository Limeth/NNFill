package cz.kybersquat.limeth.nnfill.command;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import cz.kybersquat.limeth.nnfill.Vector2i;
import cz.kybersquat.limeth.nnfill.command.converters.Vector2iConverter;
import cz.kybersquat.limeth.nnfill.command.validators.ExistingFile;
import cz.kybersquat.limeth.nnfill.command.validators.MissingDirectory;
import cz.kybersquat.limeth.nnfill.simulation.training.layers.TrainingSimulation;
import cz.kybersquat.limeth.nnfill.world.Face;
import cz.kybersquat.limeth.nnfill.world.shape.WorldShape;

import java.io.File;

/**
 * @author Limeth
 */
@Parameters(commandNames = "createSimulation", commandDescription = "Creates a Simulation file")
public class CreateSimulationSubcommand implements Subcommand
{
	@Parameter(names = {"-w", "--world", "--world-shape"}, required = true, validateValueWith = ExistingFile.class,
			description = "The shape of the world, will read from the default input stream if not specified")
	private File worldShapeFile;

	@Parameter(names = {"-l", "--location", "--origin", "--creature-location", "--creature-origin"}, required = true,
			converter = Vector2iConverter.class, arity = 1, description = "The starting location of the creature. Ex: 10,5")
	private Object creatureOrigin;

	@Parameter(names = {"-f", "--face", "--creature-face"}, description = "The facing of the creature")
	private Face creatureFace = Face.NORTH;

	@Parameter(names = {"-o", "--output"}, validateValueWith = MissingDirectory.class,
			description = "The output file. If this argument is omitted, the output goes to the console")
	private File outputFile;

	public File getWorldShapeFile()
	{
		return worldShapeFile;
	}

	public Vector2i getCreatureOrigin()
	{
		return (Vector2i) creatureOrigin;
	}

	public Face getCreatureFace()
	{
		return creatureFace;
	}

	public File getOutputFile()
	{
		return outputFile;
	}

	@Override
	public void execute(JCommander jCommander)
	{
		WorldShape worldShape = SubcommandUtil.load(WorldShape.class, worldShapeFile);
		TrainingSimulation simulation = new TrainingSimulation(worldShape, (Vector2i) creatureOrigin, creatureFace);

		SubcommandUtil.save(simulation, outputFile);
	}
}
