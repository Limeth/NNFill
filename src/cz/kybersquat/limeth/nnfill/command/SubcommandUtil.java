package cz.kybersquat.limeth.nnfill.command;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.stream.JsonReader;
import cz.kybersquat.limeth.nnfill.json.NNFillAdapterFactory;
import cz.kybersquat.limeth.nnfill.simulation.AbstractSimulation;
import cz.kybersquat.limeth.nnfill.simulation.Simulation;
import cz.kybersquat.limeth.nnfill.simulation.SimulationRenderer;
import cz.kybersquat.limeth.nnfill.simulation.training.AbstractTraining;
import cz.kybersquat.limeth.nnfill.simulation.training.BrainPerformance;
import cz.kybersquat.limeth.nnfill.simulation.training.UpgradeHandler;
import cz.kybersquat.limeth.nnfill.world.CreatureBrain;
import neuroNet.limeth.network.NeuralNetwork;

import java.io.*;
import java.nio.file.Paths;

/**
 * @author Limeth
 */
public final class SubcommandUtil
{
	private static final Gson GSON = NNFillAdapterFactory.createBuilder().serializeNulls().setPrettyPrinting().create();

	private SubcommandUtil() {}

	private static void saveUnsafe(Object element, File outputFile) throws IOException
	{
		Appendable appendable;
		boolean close;

		if(outputFile != null)
		{
			if(!outputFile.isFile())
			{
				if(!outputFile.exists())
				{
					if(outputFile.getParentFile() != null && !outputFile.getParentFile().exists())
						if(!outputFile.getParentFile().mkdirs())
							throw new IOException("Could not create directories for file '" + outputFile + "'.");
				}
				else if(!outputFile.delete())
					throw new IOException("Could not delete previous file '" + outputFile + "'.");

				if(!outputFile.createNewFile())
					throw new IOException("Could not create file '" + outputFile + "'.");
			}

			appendable = new FileWriter(outputFile);
			close = true;
		}
		else
		{
			appendable = System.out;
			close = false;
		}

		if(element instanceof JsonElement)
			GSON.toJson((JsonElement) element, appendable);
		else
			GSON.toJson(element, appendable);

		if(close)
			((Closeable) appendable).close();
	}

	public static void save(Object element, File outputFile)
	{
		try
		{
			saveUnsafe(element, outputFile);
		}
		catch(IOException e)
		{
			throw new RuntimeException("Could not save Jsonable to file '" + outputFile + "'.", e);
		}
	}

	private static <T> T loadUnsafe(Class<T> clazz, File inputFile) throws IOException
	{
		Reader input;

		if(inputFile != null)
			input = new FileReader(inputFile);
		else
			input = new InputStreamReader(System.in);

		JsonReader reader = new JsonReader(input);
		T result = GSON.fromJson(reader, clazz);

		reader.close();

		return result;
	}

	public static <T> T load(Class<T> clazz, File inputFile)
	{
		try
		{
			return loadUnsafe(clazz, inputFile);
		}
		catch(IOException e)
		{
			throw new RuntimeException("Could not parse json from file '" + inputFile + "' for class '" + clazz.getCanonicalName() + "'.", e);
		}
	}

	public static class TUISimulationRenderer implements SimulationRenderer, Closeable
	{
		private final boolean closeOutput;
		private final PrintStream output;

		private TUISimulationRenderer(PrintStream output, boolean closeOutput)
		{
			this.output = output;
			this.closeOutput = closeOutput;
		}

		public TUISimulationRenderer(File outputFile) throws FileNotFoundException
		{
			closeOutput = outputFile != null;
			output = outputFile != null ? new PrintStream(outputFile) : System.out;
		}

		@Override
		public void render(AbstractSimulation simulation)
		{
			output.println(simulation.getWorld().getDebug());
		}

		public PrintStream getOutput()
		{
			return output;
		}

		@Override
		public void close()
		{
			if(closeOutput)
				output.close();
		}
	}

	public static class TUIUpgradeHandler implements UpgradeHandler, Closeable
	{
		private final File outputDirectory;
		private final boolean closeOutput;
		private final PrintStream output;
		private final TUISimulationRenderer renderer;

		public TUIUpgradeHandler(File outputFile, File outputDirectory) throws FileNotFoundException
		{
			this.outputDirectory = outputDirectory == null ? new File(Paths.get(".").normalize().toAbsolutePath().toString()) : outputDirectory;
			closeOutput = outputFile != null;
			output = outputFile != null ? new PrintStream(outputFile) : System.out;
			renderer = new TUISimulationRenderer(output, closeOutput);
		}

		@Override
		public void onUpgrade(AbstractTraining training)
		{
			BrainPerformance resultPerformance = training.getResultPerformance();
			CreatureBrain resultBrain = resultPerformance.getBrain();
			NeuralNetwork network = resultBrain.getNetwork();
			Simulation simulation = training.getOriginalSimulation().clone();
			SimulationRenderer previousRenderer = simulation.getRenderer();

			simulation.setRenderer(renderer);
			output.println(training.getName() + ":");
			simulation.filter(resultBrain);
			output.println("New best performance: " + resultPerformance.getFitness());
			simulation.setRenderer(previousRenderer);

			File networkFile = new File(outputDirectory, training.getName() + "_network_" + resultPerformance.getFitness() + ".json");
			int i = 2;

			while(networkFile.exists())
				networkFile = new File(outputDirectory, training.getName() + "_network_" + resultPerformance.getFitness() + "_" + i + ".json");

			save(network, networkFile);
		}

		public PrintStream getOutput()
		{
			return output;
		}

		@Override
		public void close()
		{
			if(closeOutput)
				output.close();
		}
	}
}
