package cz.kybersquat.limeth.nnfill.command;

import com.beust.jcommander.*;
import com.beust.jcommander.Parameter;

import java.lang.reflect.*;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * @author Limeth
 */
public class JCommanderFixed extends JCommander
{
	public JCommanderFixed()
	{
	}

	public JCommanderFixed(Object object)
	{
		super(object);
	}

	public JCommanderFixed(Object object, ResourceBundle bundle)
	{
		super(object, bundle);
	}

	public JCommanderFixed(Object object, ResourceBundle bundle, String... args)
	{
		super(object, bundle, args);
	}

	public JCommanderFixed(Object object, String... args)
	{
		super(object, args);
	}

	private <T> T invokeParentMethod(String name, Class<?>[] parameterTypes, Object... parameters)
	{
		return invokeMethod(getClass().getSuperclass(), this, name, parameterTypes, parameters);
	}

	private <T> T invokeMethod(Class<?> methodClass, Object instance, String name, Class<?>[] parameterTypes, Object... parameters)
	{
		try
		{
			Method method = methodClass.getDeclaredMethod(name, parameterTypes);

			if(!method.isAccessible())
				method.setAccessible(true);

			return (T) method.invoke(instance, parameters);
		}
		catch(IllegalAccessException | NoSuchMethodException | InvocationTargetException e)
		{
			throw new RuntimeException(methodClass + " " + instance + " " + name + " " + Arrays.toString(parameterTypes), e);
		}
	}

	private <T> T getParentField(String name)
	{
		return getParentFieldOf(this, name);
	}

	private <T> T getParentFieldOf(Object instance, String name)
	{
		return getFieldOf(instance.getClass().getSuperclass(), instance, name);
	}

	private <T> T getFieldOf(Class<?> clazz, Object instance, String name)
	{
		try
		{
			Field field = (clazz != null ? clazz : instance.getClass()).getDeclaredField(name);

			if(!field.isAccessible())
				field.setAccessible(true);

			return (T) field.get(instance);
		}
		catch(IllegalAccessException | NoSuchFieldException e)
		{
			throw new RuntimeException(e);
		}
	}

	private <T> void setParentField(String name, T value)
	{
		try
		{
			Field field = getClass().getSuperclass().getDeclaredField(name);

			if(!field.isAccessible())
				field.setAccessible(true);

			field.set(this, value);
		}
		catch(IllegalAccessException | NoSuchFieldException e)
		{
			throw new RuntimeException(e);
		}
	}

	public void validateOptions()
	{
		invokeParentMethod("validateOptions", new Class<?>[0]);
	}

	/**
	 * Parse and validate the command line parameters.
	 */
	public void parse(String... args) {
		parse(true /* validate */, args);
	}

	/**
	 * Parse the command line parameters without validating them.
	 */
	public void parseWithoutValidation(String... args) {
		parse(false /* no validation */, args);
	}

	private void parse(boolean validate, String... args) {
		StringBuilder sb = new StringBuilder("Parsing \"");
		sb.append(((StringBuilder) invokeParentMethod("join", new Class<?>[] {Object[].class}, new Object[] {args})).append("\"\n  with:")
		    .append(((StringBuilder) invokeParentMethod("join", new Class<?>[] {Object[].class}, new Object[] {((List<Object>) getParentField("m_objects")).toArray()}))));
		invokeParentMethod("p", new Class<?>[] {String.class}, sb.toString());

		if (getParentField("m_descriptions") == null) invokeParentMethod("createDescriptions", new Class<?>[0]);
		invokeParentMethod("initializeDefaultValues", new Class<?>[0]);
		parseValues(invokeParentMethod("expandArgs", new Class<?>[]{String[].class}, new Object[] {args}), validate);
		if (validate) invokeParentMethod("validateOptions", new Class<?>[0]);
	}

	/**
	 * Main method that parses the values and initializes the fields accordingly.
	 */
	private void parseValues(String[] args, boolean validate) {
		// This boolean becomes true if we encounter a command, which indicates we need
		// to stop parsing (the parsing of the command will be done in a sub JCommander
		// object)
		boolean commandParsed = false;
		int i = 0;
		boolean isDashDash = false; // once we encounter --, everything goes into the main parameter
		while (i < args.length && ! commandParsed) {
			String arg = args[i];
			String a = invokeParentMethod("trim", new Class<?>[]{String.class}, arg);
			args[i] = a;
			invokeParentMethod("p", new Class<?>[] {String.class}, "Parsing arg: " + a);

			JCommander jc = invokeParentMethod("findCommandByAlias", new Class<?>[] {String.class}, arg);
			int increment = 1;
			if (! isDashDash && ! "--".equals(a) && (boolean) invokeParentMethod("isOption", new Class<?>[] {String[].class, String.class}, args, a) && jc == null) {
				//
				// Option
				//
				ParameterDescription pd = invokeParentMethod("findParameterDescription", new Class<?>[] {String.class}, a);

				if (pd != null) {
					if (pd.getParameter().password()) {
						//
						// Password option, use the Console to retrieve the password
						//
						char[] password = invokeParentMethod("readPassword", new Class<?>[] {String.class, boolean.class}, pd.getDescription(), pd.getParameter().echoInput());
						pd.addValue(new String(password));
						((Map<Parameterized, ParameterDescription>) getParentField("m_requiredFields")).remove(pd.getParameterized());
					} else {
						if (pd.getParameter().variableArity()) {
							//
							// Variable arity?
							//
							increment = invokeParentMethod("processVariableArity", new Class<?>[] {String[].class, int.class, ParameterDescription.class}, args, i, pd);
						} else {
							//
							// Regular option
							//
							Class<?> fieldType = pd.getParameterized().getType();

							// Boolean, set to true as soon as we see it, unless it specified
							// an arity of 1, in which case we need to read the next value
							if ((fieldType == boolean.class || fieldType == Boolean.class)
							    && pd.getParameter().arity() == -1) {
								pd.addValue("true");
								((Map<Parameterized, ParameterDescription>) getParentField("m_requiredFields")).remove(pd.getParameterized());
							} else {
								increment = invokeParentMethod("processFixedArity", new Class<?>[] {String[].class, int.class, ParameterDescription.class, Class.class}, args, i, pd, fieldType);
							}
							// If it's a help option, remember for later
							if (pd.isHelp()) {
								setParentField("m_helpWasSpecified", true);
							}
						}
					}
				} else {
					if ((boolean) getParentField("m_acceptUnknownOptions")) {
						((List<String>) getParentField("m_unknownArgs")).add(arg);
						i++;
						while (i < args.length && ! ((boolean) invokeParentMethod("isOption", new Class<?>[] {String[].class, String.class}, args, args[i]))) {
							((List<String>) getParentField("m_unknownArgs")).add(args[i++]);
						}
						increment = 0;
					} else {
						throw new ParameterException("Unknown option: " + arg);
					}
				}
			}
			else {
				//
				// Main parameter
				//
				if (! Strings.isStringEmpty(arg)) {
					if ("--".equals(arg)) {
						isDashDash = true;
						a = invokeParentMethod("trim", new Class<?>[]{String.class}, args[++i]);
					}
					if (((Map<?, JCommander>) getParentField("m_commands")).isEmpty()) {
						//
						// Regular (non-command) parsing
						//
						List mp = invokeParentMethod("getMainParameter", new Class<?>[] {String.class}, arg);
						String value = a; // If there's a non-quoted version, prefer that one
						Object convertedValue = value;

						if (((Parameterized) getParentField("m_mainParameter")).getGenericType() instanceof ParameterizedType) {
							ParameterizedType p = (ParameterizedType) ((Parameterized) getParentField("m_mainParameter")).getGenericType();
							Type cls = p.getActualTypeArguments()[0];
							if (cls instanceof Class) {
								convertedValue = convertValue(((Parameterized) getParentField("m_mainParameter")), (Class) cls, value);
							}
						}

						ParameterDescription.validateParameter((ParameterDescription) getParentField("m_mainParameterDescription"),
						                                       ((Parameter) getParentField("m_mainParameterAnnotation")).validateWith(),
						                                       "Default", value);

						((ParameterDescription) getParentField("m_mainParameterDescription")).setAssigned(true);
						mp.add(convertedValue);
					}
					else {
						//
						// Command parsing
						//
						if (jc == null && validate) {
							throw new MissingCommandException("Expected a command, got " + arg);
						} else if (jc != null){
							setParentField("m_parsedCommand", getFieldOf(null, getFieldOf(JCommander.class, jc, "m_programName"), "m_name"));
							setParentField("m_parsedAlias", arg); //preserve the original form

							// Found a valid command, ask it to parse the remainder of the arguments.
							// Setting the boolean commandParsed to true will force the current
							// loop to end.
							invokeMethod(JCommander.class, jc, "parse", new Class<?>[] {boolean.class, String[].class}, validate, invokeParentMethod("subArray", new Class<?>[]{String[].class, int.class}, args, i + 1));
							commandParsed = true;
						}
					}
				}
			}
			i += increment;
		}

		// Mark the parameter descriptions held in m_fields as assigned
		for (ParameterDescription parameterDescription : ((Map<?, ParameterDescription>) getParentField("m_descriptions")).values()) {
			if (parameterDescription.isAssigned()) {
				((Map<Parameterized, ParameterDescription>) getParentField("m_fields")).get(parameterDescription.getParameterized()).setAssigned(true);
			}
		}

	}
}
