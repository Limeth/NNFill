package cz.kybersquat.limeth.nnfill.command;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import cz.kybersquat.limeth.nnfill.command.converters.FilterLayerConverter;
import cz.kybersquat.limeth.nnfill.command.validators.ExistingFile;
import cz.kybersquat.limeth.nnfill.command.validators.MissingDirectory;
import cz.kybersquat.limeth.nnfill.simulation.AbstractSimulation;
import cz.kybersquat.limeth.nnfill.simulation.FilterLayer;
import cz.kybersquat.limeth.nnfill.simulation.Simulation;
import cz.kybersquat.limeth.nnfill.simulation.training.Training;
import neuroNet.limeth.network.NeuralNetwork;

import java.io.File;
import java.util.List;

/**
 * @author Limeth
 */
@Parameters(commandNames = "createTraining", commandDescription = "Creates a Training file")
public class CreateTrainingSubcommand implements Subcommand
{
	@Parameter(names = {"--name"}, description = "The name of the training")
	private String name;

	@Parameter(names = {"-s", "--simulation"}, required = true, validateValueWith = ExistingFile.class, description = "The simulation file, will read from the default input stream if not specified")
	private File simulationFile;

	@Parameter(names = {"-n", "--network", "--original-network"}, required = true, validateValueWith = ExistingFile.class, description = "The original network file, will read from the default input stream if not specified")
	private File originalNetworkFile;

	@Parameter(names = {"-t", "--threads"}, description = "The amount of threads to use")
	private int threadAmount = 4;

	@Parameter(names = {"-f", "--fitness", "--required-fitness"}, description = "The required fitness to finish the training in range (0; 1>")
	private double requiredFitness = 1;

	@Parameter(names = {"-l", "--layers", "--filter-layers"}, converter = FilterLayerConverter.class,
			variableArity = true, description = "CreatureBrain-modifying layers for each iteration of the training")
	private List<FilterLayer> filterLayers;

	@Parameter(names = {"-o", "--output"}, validateValueWith = MissingDirectory.class,
			description = "The output file. If this argument is omitted, the output goes to the console")
	private File outputFile;

	@Override
	public void execute(JCommander jCommander) throws ParameterException
	{
		if(threadAmount <= 0)
			throw new ParameterException("The thread amount must be larger than 0!");
		if(requiredFitness <= 0 || requiredFitness > 1)
			throw new ParameterException("The required fitness must be in range (0; 1>!");

		NeuralNetwork originalNetwork = SubcommandUtil.load(NeuralNetwork.class, originalNetworkFile);
		Simulation simulation = SubcommandUtil.load(AbstractSimulation.class, simulationFile);

		for(int i = filterLayers.size() - 1; i >= 0; i--)
		{
			FilterLayer filterLayer = filterLayers.get(i);

			filterLayer.setChild(simulation);

			simulation = filterLayer;
		}

		Training training = new Training(name, originalNetwork, simulation, threadAmount, requiredFitness);

		SubcommandUtil.save(training, outputFile);
	}
}
