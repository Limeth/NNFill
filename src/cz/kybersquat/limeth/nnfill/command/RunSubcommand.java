package cz.kybersquat.limeth.nnfill.command;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import cz.kybersquat.limeth.nnfill.fx.NNFApplication;
import cz.kybersquat.limeth.nnfill.command.validators.ExistingFile;
import cz.kybersquat.limeth.nnfill.command.validators.MissingDirectory;
import cz.kybersquat.limeth.nnfill.simulation.AbstractSimulation;
import cz.kybersquat.limeth.nnfill.world.CreatureBrain;
import javafx.application.Application;
import neuroNet.limeth.network.NeuralNetwork;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * @author Limeth
 */
@Parameters(commandNames = "run", commandDescription = "Run a simulation")
public class RunSubcommand implements Subcommand
{
	@Parameter(names = {"-s", "--simulation"}, required = true, validateValueWith = ExistingFile.class, description = "The Simulation file, will read from the default input stream if not specified")
	private File simulationFile;

	@Parameter(names = {"-n", "--network", "--neural-network"}, required = true, validateValueWith = ExistingFile.class, description = "The NeuralNetwork file to use in the simulation as a brain, will read from the default input stream if not specified")
	private File networkFile;

	@Parameter(names = {"-g", "--gui"}, description = "Display Simulation progress in a JavaFX graphical interface")
	private boolean gui;

	@Parameter(names = {"-o", "--output"}, validateValueWith = MissingDirectory.class,
			description = "The output file. If this argument is omitted, the output goes to the console")
	private File outputFile;

	@Override
	public void execute(JCommander jCommander) throws ParameterException
	{
		AbstractSimulation simulation = SubcommandUtil.load(AbstractSimulation.class, simulationFile);
		NeuralNetwork network = SubcommandUtil.load(NeuralNetwork.class, networkFile);

		if(gui)
		{
			NNFApplication.setSimulation(simulation);
			NNFApplication.setNetwork(network);
			NNFApplication.setOutputFile(outputFile);

			Application.launch(NNFApplication.class);
		}
		else
			try
			{
				launchTUISimulation(simulation, network, outputFile);
			}
			catch(FileNotFoundException e)
			{
				throw new RuntimeException(e);
			}
	}

	private static void launchTUISimulation(AbstractSimulation simulation, NeuralNetwork network, File outputFile) throws FileNotFoundException
	{
		CreatureBrain brain = CreatureBrain.of(network);
		SubcommandUtil.TUISimulationRenderer renderer = new SubcommandUtil.TUISimulationRenderer(outputFile);

		simulation.setRenderer(renderer);
		simulation.filter(brain);
		renderer.close();
	}
}
