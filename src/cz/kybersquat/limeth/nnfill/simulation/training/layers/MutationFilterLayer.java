package cz.kybersquat.limeth.nnfill.simulation.training.layers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import cz.kybersquat.limeth.nnfill.command.converters.FilterLayerConverter;
import cz.kybersquat.limeth.nnfill.simulation.FilterLayer;
import cz.kybersquat.limeth.nnfill.simulation.Simulation;
import cz.kybersquat.limeth.nnfill.simulation.SimulationRenderer;
import cz.kybersquat.limeth.nnfill.simulation.training.BrainPerformance;
import cz.kybersquat.limeth.nnfill.world.CreatureBrain;
import cz.kybersquat.limeth.nnfill.world.World;
import neuroNet.limeth.network.NeuralConnection;
import neuroNet.limeth.network.NeuralNetwork;

import java.util.List;
import java.util.Random;

/**
 * @author Limeth
 */
public class MutationFilterLayer implements FilterLayer
{
	private static final String JSON_FIELD_SIMULATION     = "simulation";
	private static final String JSON_FIELD_MUTATIONCHANCE = "mutationChance";
	private static final String JSON_FIELD_MUTATIONWEIGHT = "mutationWeight";
	private Random     random;
	private Simulation simulation;
	private double     mutationChance;
	private double     mutationWeight;

	public MutationFilterLayer(Simulation simulation, Random random, double mutationChance, double mutationWeight)
	{
		this.random = random;
		this.simulation = simulation;
		this.mutationChance = mutationChance;
		this.mutationWeight = mutationWeight;
	}

	public static MutationFilterLayer fromJson(JsonElement rawRoot, JsonDeserializationContext context)
	{
		JsonObject root = rawRoot.getAsJsonObject();
		Simulation simulation = context.deserialize(root.get(JSON_FIELD_SIMULATION), Simulation.class);
		double mutationChance = root.get(JSON_FIELD_MUTATIONCHANCE).getAsDouble();
		double mutationWeight = root.get(JSON_FIELD_MUTATIONWEIGHT).getAsDouble();

		return new MutationFilterLayer(simulation, new Random(), mutationChance, mutationWeight);
	}

	public static MutationFilterLayer parse(String[] args) throws FilterLayerConverter.FilterLayerParseException
	{
		if(args.length != 2)
			throw new FilterLayerConverter.FilterLayerParseException();

		try
		{
			double mutationChance = Integer.parseInt(args[0]);
			double mutationWeight = Integer.parseInt(args[1]);

			return new MutationFilterLayer(null, new Random(), mutationChance, mutationWeight);
		}
		catch(NumberFormatException e)
		{
			throw new FilterLayerConverter.FilterLayerParseException(e);
		}
	}

	public static String[] getSyntax()
	{
		return new String[] {"double-mutationChance", "double-mutationWeight"};
	}

	@Override
	public JsonElement toJson(JsonSerializationContext context)
	{
		JsonObject root = new JsonObject();

		root.add(JSON_FIELD_SIMULATION, context.serialize(simulation));
		root.addProperty(JSON_FIELD_MUTATIONCHANCE, mutationChance);
		root.addProperty(JSON_FIELD_MUTATIONWEIGHT, mutationWeight);

		return root;
	}

	private CreatureBrain mutateBrain(CreatureBrain sourceBrain)
	{
		NeuralNetwork originalNetwork = sourceBrain.getNetwork();
		NeuralNetwork mutatedNetwork = originalNetwork.clone();

		mutatedNetwork.getConnections().forEach(this::mutateWeight);

		return CreatureBrain.of(mutatedNetwork);
	}

	private void mutateWeight(NeuralConnection connection)
	{
		connection.setWeight(mutateWeight(connection.getWeight()));
	}

	private float mutateWeight(float originalWeight)
	{
		if(Math.random() > mutationChance)
			return originalWeight;

		return (float) ((random.nextBoolean() ? 1 : -1) * random.nextDouble() * mutationWeight + originalWeight);
	}

	@Override
	public List<BrainPerformance> filter(CreatureBrain brain)
	{
		CreatureBrain mutatedBrain = mutateBrain(brain);
		List<BrainPerformance> performances = simulation.filter(mutatedBrain);

		return performances;
	}

	@Override
	public Simulation getChild()
	{
		return simulation;
	}

	@Override
	public void setChild(Simulation simulation)
	{
		this.simulation = simulation;
	}

	@Override
	public Simulation getSimulation()
	{
		return simulation instanceof FilterLayer ? ((FilterLayer) simulation).getSimulation() : simulation;
	}

	@Override
	public void setSimulation(Simulation simulation)
	{
		if(this.simulation instanceof FilterLayer)
			((FilterLayer) this.simulation).setSimulation(simulation);
		else
			this.simulation = simulation;
	}

	@Override
	public SimulationRenderer getRenderer()
	{
		return getSimulation().getRenderer();
	}

	@Override
	public void setRenderer(SimulationRenderer renderer)
	{
		getSimulation().setRenderer(renderer);
	}

	@Override
	public World getWorld()
	{
		return simulation.getWorld();
	}

	@Override
	public MutationFilterLayer clone()
	{
		return new MutationFilterLayer(simulation != null ? simulation.clone() : null, new Random(), mutationChance, mutationWeight);
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(o == null || getClass() != o.getClass())
			return false;

		MutationFilterLayer that = (MutationFilterLayer) o;

		if(Double.compare(that.mutationChance, mutationChance) != 0)
			return false;
		if(Double.compare(that.mutationWeight, mutationWeight) != 0)
			return false;
		if(!simulation.equals(that.simulation))
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result;
		long temp;
		result = simulation.hashCode();
		temp = Double.doubleToLongBits(mutationChance);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(mutationWeight);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	/*	@Override
	public boolean isValid()
	{
		return false;
	}*/

/*	@Override
	public BrainPerformance getResultPerformance() throws IllegalStateException
	{
		if(mutatedBrainPerformance == null)
			throw new IllegalStateException("The filter has not finished.");

		return mutatedBrainPerformance;
	}*/

/*	@Override
	public boolean isFinished()
	{
		return mutatedBrainPerformance != null;
	}*/
}
