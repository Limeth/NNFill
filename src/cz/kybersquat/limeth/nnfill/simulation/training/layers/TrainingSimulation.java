package cz.kybersquat.limeth.nnfill.simulation.training.layers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import cz.kybersquat.limeth.nnfill.Vector2i;
import cz.kybersquat.limeth.nnfill.simulation.AbstractSimulation;
import cz.kybersquat.limeth.nnfill.simulation.SimulationRenderer;
import cz.kybersquat.limeth.nnfill.world.Face;
import cz.kybersquat.limeth.nnfill.world.shape.WorldShape;

/**
 * @author Limeth
 */
public class TrainingSimulation extends AbstractSimulation
{
	private static final String JSON_FIELD_WORLDSHAPE = "worldShape";
	private static final String JSON_FIELD_CREATUREORIGIN = "creatureOrigin";
	private static final String JSON_FIELD_CREATUREFACE = "creatureFace";
	private Vector2i creatureOrigin;
	private Face creatureFace;
	private SimulationRenderer renderer;

	public TrainingSimulation(WorldShape shape, Vector2i creatureOrigin, Face creatureFace, SimulationRenderer renderer)
	{
		super(shape);

		this.creatureOrigin = creatureOrigin;
		this.creatureFace = creatureFace;
		this.renderer = renderer;
	}

	public TrainingSimulation(WorldShape shape, Vector2i creatureOrigin, SimulationRenderer renderer)
	{
		this(shape, creatureOrigin, Face.random(), renderer);
	}

	public TrainingSimulation(WorldShape shape, Face creatureFace, SimulationRenderer renderer)
	{
		this(shape, new Vector2i(shape.getWidth() / 2, shape.getHeight() / 2), creatureFace, renderer);
	}

	public TrainingSimulation(WorldShape shape, SimulationRenderer renderer)
	{
		this(shape, new Vector2i(shape.getWidth() / 2, shape.getHeight() / 2), Face.random(), renderer);
	}

	public TrainingSimulation(WorldShape shape, Vector2i creatureOrigin, Face creatureFace)
	{
		this(shape, creatureOrigin, creatureFace, null);
	}

	public TrainingSimulation(WorldShape shape, Vector2i creatureOrigin)
	{
		this(shape, creatureOrigin, Face.random(), null);
	}

	public TrainingSimulation(WorldShape shape, Face creatureFace)
	{
		this(shape, new Vector2i(shape.getWidth() / 2, shape.getHeight() / 2), creatureFace, null);
	}

	public TrainingSimulation(WorldShape shape)
	{
		this(shape, new Vector2i(shape.getWidth() / 2, shape.getHeight() / 2), Face.random(), null);
	}

	public static TrainingSimulation fromJson(JsonElement rawRoot, JsonDeserializationContext context)
	{
		JsonObject root = rawRoot.getAsJsonObject();
		WorldShape shape = context.deserialize(root.get(JSON_FIELD_WORLDSHAPE), WorldShape.class);
		Vector2i creatureOrigin = context.deserialize(root.get(JSON_FIELD_CREATUREORIGIN), Vector2i.class);
		Face creatureFace = context.deserialize(root.get(JSON_FIELD_CREATUREFACE), Face.class);

		return new TrainingSimulation(shape, creatureOrigin, creatureFace);
	}

	@Override
	public JsonElement toJson(JsonSerializationContext context)
	{
		JsonObject root = new JsonObject();

		root.add(JSON_FIELD_WORLDSHAPE, context.serialize(getWorld().getShape()));
		root.add(JSON_FIELD_CREATUREORIGIN, context.serialize(creatureOrigin));
		root.add(JSON_FIELD_CREATUREFACE, context.serialize(creatureFace));

		return root;
	}

	@Override
	public Vector2i getCreatureOrigin()
	{
		return creatureOrigin.clone();
	}

	@Override
	public SimulationRenderer getRenderer()
	{
		return renderer;
	}

	@Override
	public void setRenderer(SimulationRenderer renderer)
	{
		this.renderer = renderer;
	}

	@Override
	public TrainingSimulation clone()
	{
		return new TrainingSimulation(getWorld().getShape(), creatureOrigin.clone(), creatureFace, renderer);
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(o == null || getClass() != o.getClass())
			return false;

		TrainingSimulation that = (TrainingSimulation) o;

		if(creatureFace != that.creatureFace)
			return false;
		if(!creatureOrigin.equals(that.creatureOrigin))
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = creatureOrigin.hashCode();
		result = 31 * result + creatureFace.hashCode();
		return result;
	}
}
