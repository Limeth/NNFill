package cz.kybersquat.limeth.nnfill.simulation.training.layers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import cz.kybersquat.limeth.nnfill.command.converters.FilterLayerConverter;
import cz.kybersquat.limeth.nnfill.simulation.FilterLayer;
import cz.kybersquat.limeth.nnfill.simulation.Simulation;
import cz.kybersquat.limeth.nnfill.simulation.SimulationRenderer;
import cz.kybersquat.limeth.nnfill.simulation.training.BrainPerformance;
import cz.kybersquat.limeth.nnfill.world.CreatureBrain;
import cz.kybersquat.limeth.nnfill.world.World;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Limeth
 */
public class ExpansionFilterLayer implements FilterLayer
{
	private static final String JSON_FIELD_SIMULATION = "simulation";
	private static final String JSON_FIELD_AMOUNT      = "amount";
	private Simulation simulation;
	private int        amount;

	public ExpansionFilterLayer(Simulation simulation, int amount)
	{
		this.simulation = simulation;
		this.amount = amount;
	}

	public static ExpansionFilterLayer fromJson(JsonElement rawRoot, JsonDeserializationContext context)
	{
		JsonObject root = rawRoot.getAsJsonObject();
		Simulation simulation = context.deserialize(root.get(JSON_FIELD_SIMULATION), Simulation.class);
		int amount = root.get(JSON_FIELD_AMOUNT).getAsInt();

		return new ExpansionFilterLayer(simulation, amount);
	}

	public static ExpansionFilterLayer parse(String[] args) throws FilterLayerConverter.FilterLayerParseException
	{
		if(args.length != 1)
			throw new FilterLayerConverter.FilterLayerParseException();

		try
		{
			int amount = Integer.parseInt(args[0]);

			return new ExpansionFilterLayer(null, amount);
		}
		catch(NumberFormatException e)
		{
			throw new FilterLayerConverter.FilterLayerParseException(e);
		}
	}

	public static String[] getSyntax()
	{
		return new String[] {"int-amount"};
	}

	@Override
	public JsonElement toJson(JsonSerializationContext context)
	{
		JsonObject root = new JsonObject();

		root.add(JSON_FIELD_SIMULATION, context.serialize(simulation));
		root.addProperty(JSON_FIELD_AMOUNT, amount);

		return root;
	}

	@Override
	public List<BrainPerformance> filter(CreatureBrain brain)
	{
		List<BrainPerformance> performances = new ArrayList<>();
		int iteration = 0;

		while(iteration < amount)
		{
			List<BrainPerformance> performance = simulation.filter(brain);

			performances.addAll(performance);

			iteration++;
		}

		return performances;
	}

	@Override
	public World getWorld()
	{
		return simulation.getWorld();
	}

	@Override
	public Simulation getChild()
	{
		return simulation;
	}

	@Override
	public void setChild(Simulation simulation)
	{
		this.simulation = simulation;
	}

	@Override
	public Simulation getSimulation()
	{
		return simulation instanceof FilterLayer ? ((FilterLayer) simulation).getSimulation() : simulation;
	}

	@Override
	public void setSimulation(Simulation simulation)
	{
		if(this.simulation instanceof FilterLayer)
			((FilterLayer) this.simulation).setSimulation(simulation);
		else
			this.simulation = simulation;
	}

	@Override
	public SimulationRenderer getRenderer()
	{
		return getSimulation().getRenderer();
	}

	@Override
	public void setRenderer(SimulationRenderer renderer)
	{
		getSimulation().setRenderer(renderer);
	}

	@Override
	public ExpansionFilterLayer clone()
	{
		return new ExpansionFilterLayer(simulation != null ? simulation.clone() : null, amount);
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(o == null || getClass() != o.getClass())
			return false;

		ExpansionFilterLayer that = (ExpansionFilterLayer) o;

		if(amount != that.amount)
			return false;
		if(!simulation.equals(that.simulation))
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = simulation.hashCode();
		result = 31 * result + amount;
		return result;
	}
}
