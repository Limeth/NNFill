package cz.kybersquat.limeth.nnfill.simulation.training.layers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import cz.kybersquat.limeth.nnfill.command.converters.FilterLayerConverter;
import cz.kybersquat.limeth.nnfill.simulation.FilterLayer;
import cz.kybersquat.limeth.nnfill.simulation.Simulation;
import cz.kybersquat.limeth.nnfill.simulation.SimulationRenderer;
import cz.kybersquat.limeth.nnfill.simulation.training.BrainPerformance;
import cz.kybersquat.limeth.nnfill.world.CreatureBrain;
import cz.kybersquat.limeth.nnfill.world.World;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Limeth
 */
public class LimitationFilterLayer implements FilterLayer
{
	private static final String JSON_FIELD_SIMULATION = "simulation";
	private static final String JSON_FIELD_MAXAMOUNT   = "maxAmount";
	private Simulation simulation;
	private int        maxAmount;

	public LimitationFilterLayer(Simulation simulation, int maxAmount)
	{
		this.simulation = simulation;
		this.maxAmount = maxAmount;
	}

	public static LimitationFilterLayer fromJson(JsonElement rawRoot, JsonDeserializationContext context)
	{
		JsonObject root = rawRoot.getAsJsonObject();
		Simulation simulation = context.deserialize(root.get(JSON_FIELD_SIMULATION), Simulation.class);
		int maxAmount = root.get(JSON_FIELD_MAXAMOUNT).getAsInt();

		return new LimitationFilterLayer(simulation, maxAmount);
	}

	public static LimitationFilterLayer parse(String[] args) throws FilterLayerConverter.FilterLayerParseException
	{
		if(args.length != 1)
			throw new FilterLayerConverter.FilterLayerParseException();

		try
		{
			int maxAmount = Integer.parseInt(args[0]);

			return new LimitationFilterLayer(null, maxAmount);
		}
		catch(NumberFormatException e)
		{
			throw new FilterLayerConverter.FilterLayerParseException(e);
		}
	}

	public static String[] getSyntax()
	{
		return new String[] {"int-maxAmount"};
	}

	@Override
	public JsonElement toJson(JsonSerializationContext context)
	{
		JsonObject root = new JsonObject();

		root.add(JSON_FIELD_SIMULATION, context.serialize(simulation));
		root.addProperty(JSON_FIELD_MAXAMOUNT, maxAmount);

		return root;
	}

	@Override
	public List<BrainPerformance> filter(CreatureBrain brain)
	{
		List<BrainPerformance> performances = simulation.filter(brain);

		Collections.sort(performances, Collections.reverseOrder());

		return performances.parallelStream().limit(maxAmount).collect(Collectors.toList());
	}

	@Override
	public Simulation getChild()
	{
		return simulation;
	}

	@Override
	public void setChild(Simulation simulation)
	{
		this.simulation = simulation;
	}

	@Override
	public Simulation getSimulation()
	{
		return simulation instanceof FilterLayer ? ((FilterLayer) simulation).getSimulation() : simulation;
	}

	@Override
	public void setSimulation(Simulation simulation)
	{
		if(this.simulation instanceof FilterLayer)
			((FilterLayer) this.simulation).setSimulation(simulation);
		else
			this.simulation = simulation;
	}

	@Override
	public SimulationRenderer getRenderer()
	{
		return getSimulation().getRenderer();
	}

	@Override
	public void setRenderer(SimulationRenderer renderer)
	{
		getSimulation().setRenderer(renderer);
	}

	@Override
	public World getWorld()
	{
		return simulation.getWorld();
	}

	public int getMaxAmount()
	{
		return maxAmount;
	}

	public void setMaxAmount(int maxAmount)
	{
		this.maxAmount = maxAmount;
	}

	@Override
	public LimitationFilterLayer clone()
	{
		return new LimitationFilterLayer(simulation != null ? simulation.clone() : null, maxAmount);
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(o == null || getClass() != o.getClass())
			return false;

		LimitationFilterLayer that = (LimitationFilterLayer) o;

		if(maxAmount != that.maxAmount)
			return false;
		if(!simulation.equals(that.simulation))
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = simulation.hashCode();
		result = 31 * result + maxAmount;
		return result;
	}
}
