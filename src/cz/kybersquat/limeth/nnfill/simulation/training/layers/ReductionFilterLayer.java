package cz.kybersquat.limeth.nnfill.simulation.training.layers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import cz.kybersquat.limeth.nnfill.command.converters.FilterLayerConverter;
import cz.kybersquat.limeth.nnfill.simulation.FilterLayer;
import cz.kybersquat.limeth.nnfill.simulation.Simulation;
import cz.kybersquat.limeth.nnfill.simulation.SimulationRenderer;
import cz.kybersquat.limeth.nnfill.simulation.training.BrainPerformance;
import cz.kybersquat.limeth.nnfill.world.CreatureBrain;
import cz.kybersquat.limeth.nnfill.world.World;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Limeth
 */
public class ReductionFilterLayer implements FilterLayer
{
	private static final String JSON_FIELD_SIMULATION     = "simulation";
	private static final String JSON_FIELD_REQUIREDFITNESS = "requiredFitness";
	private Simulation simulation;
	private double     requiredFitness;

	public ReductionFilterLayer(Simulation simulation, double requiredFitness)
	{
		this.simulation = simulation;
		this.requiredFitness = requiredFitness;
	}

	public static ReductionFilterLayer fromJson(JsonElement rawRoot, JsonDeserializationContext context)
	{
		JsonObject root = rawRoot.getAsJsonObject();
		Simulation simulation = context.deserialize(root.get(JSON_FIELD_SIMULATION), Simulation.class);
		double requiredFitness = root.get(JSON_FIELD_REQUIREDFITNESS).getAsDouble();

		return new ReductionFilterLayer(simulation, requiredFitness);
	}

	public static ReductionFilterLayer parse(String[] args) throws FilterLayerConverter.FilterLayerParseException
	{
		if(args.length != 1)
			throw new FilterLayerConverter.FilterLayerParseException();

		try
		{
			double requiredFitness = Integer.parseInt(args[0]);

			return new ReductionFilterLayer(null, requiredFitness);
		}
		catch(NumberFormatException e)
		{
			throw new FilterLayerConverter.FilterLayerParseException(e);
		}
	}

	public static String[] getSyntax()
	{
		return new String[] {"double-requiredFitness"};
	}

	@Override
	public JsonElement toJson(JsonSerializationContext context)
	{
		JsonObject root = new JsonObject();

		root.add(JSON_FIELD_SIMULATION, context.serialize(simulation));
		root.addProperty(JSON_FIELD_REQUIREDFITNESS, requiredFitness);

		return root;
	}

	@Override
	public List<BrainPerformance> filter(CreatureBrain brain)
	{
		List<BrainPerformance> performances = simulation.filter(brain);

		Collections.sort(performances, Collections.reverseOrder());

		return performances.parallelStream().filter(this::isAcceptable).collect(Collectors.toList());
	}

	private boolean isAcceptable(BrainPerformance performance)
	{
		return performance.getFitness() >= requiredFitness;
	}

	@Override
	public Simulation getChild()
	{
		return simulation;
	}

	@Override
	public void setChild(Simulation simulation)
	{
		this.simulation = simulation;
	}

	@Override
	public Simulation getSimulation()
	{
		return simulation instanceof FilterLayer ? ((FilterLayer) simulation).getSimulation() : simulation;
	}

	@Override
	public void setSimulation(Simulation simulation)
	{
		if(this.simulation instanceof FilterLayer)
			((FilterLayer) this.simulation).setSimulation(simulation);
		else
			this.simulation = simulation;
	}

	@Override
	public SimulationRenderer getRenderer()
	{
		return getSimulation().getRenderer();
	}

	@Override
	public void setRenderer(SimulationRenderer renderer)
	{
		getSimulation().setRenderer(renderer);
	}

	@Override
	public World getWorld()
	{
		return simulation.getWorld();
	}

	public double getRequiredFitness()
	{
		return requiredFitness;
	}

	public void setRequiredFitness(double requiredFitness)
	{
		this.requiredFitness = requiredFitness;
	}

	@Override
	public ReductionFilterLayer clone()
	{
		return new ReductionFilterLayer(simulation != null ? simulation.clone() : null, requiredFitness);
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(o == null || getClass() != o.getClass())
			return false;

		ReductionFilterLayer that = (ReductionFilterLayer) o;

		if(Double.compare(that.requiredFitness, requiredFitness) != 0)
			return false;
		if(!simulation.equals(that.simulation))
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result;
		long temp;
		result = simulation.hashCode();
		temp = Double.doubleToLongBits(requiredFitness);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
}
