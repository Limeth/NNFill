package cz.kybersquat.limeth.nnfill.simulation.training;

import cz.kybersquat.limeth.nnfill.simulation.Simulation;
import cz.kybersquat.limeth.nnfill.world.CreatureBrain;

import java.util.Collections;
import java.util.List;

/**
 * @author Limeth
 */
public class TrainingThread extends Thread
{
	private final AbstractTraining training;
//	private final Filterable       filterLayer;
	private final Simulation       simulation;
	private       CreatureBrain    currentBrain;

	public TrainingThread(String name, AbstractTraining training, Simulation simulation, CreatureBrain currentBrain)
	{
		super(name);

		this.training = training;
		this.simulation = simulation.clone();
		this.currentBrain = currentBrain.clone();
	}

	@Override
	public void run()
	{
		while(!training.isFinished())
		{
			List<BrainPerformance> currentPerformances = simulation.filter(currentBrain);

			Collections.sort(currentPerformances);

			BrainPerformance currentBestPerformance = currentPerformances.get(currentPerformances.size() - 1);

			synchronized(training.getLock())
			{
				BrainPerformance resultPerformance = training.getResultPerformanceUnlocked();

				if(resultPerformance == null || currentBestPerformance.getFitness() > resultPerformance.getFitness())
				{
					training.setResultPerformanceUnlocked(currentBestPerformance);

					resultPerformance = currentBestPerformance;

					training.onUpgrade();
				}

				//training.getPerformances().add(currentBestPerformance);

				CreatureBrain bestBrain = resultPerformance.getBrain();

				if(!currentBrain.equals(bestBrain))
					currentBrain = bestBrain;
			}
		}
	}
}
