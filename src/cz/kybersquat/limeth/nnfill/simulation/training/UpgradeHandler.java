package cz.kybersquat.limeth.nnfill.simulation.training;

/**
 * @author Limeth
 */
public interface UpgradeHandler
{
	void onUpgrade(AbstractTraining training);
}
