package cz.kybersquat.limeth.nnfill.simulation.training;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import cz.kybersquat.limeth.nnfill.simulation.Simulation;
import neuroNet.limeth.network.NeuralNetwork;

/**
 * @author Limeth
 */
public class Training extends AbstractTraining
{
	private static final String JSON_FIELD_REQUIREDFITNESS = "requiredFitness";
	private Double requiredFitness;

	public Training(String name, NeuralNetwork originalNetwork, Simulation simulation, int threadAmount, Double requiredFitness)
	{
		super(name, originalNetwork, simulation, threadAmount);

		this.requiredFitness = requiredFitness;
	}

	public Training(JsonElement rawRoot, JsonDeserializationContext context)
	{
		super(rawRoot, context);

		JsonObject root = rawRoot.getAsJsonObject();
		requiredFitness = root.has(JSON_FIELD_REQUIREDFITNESS) && !root.get(JSON_FIELD_REQUIREDFITNESS).isJsonNull()
		                  ? root.get(JSON_FIELD_REQUIREDFITNESS).getAsDouble() : null;
	}

	@Override
	public JsonObject toJson(JsonSerializationContext context)
	{
		JsonObject root = super.toJson(context);

		root.addProperty(JSON_FIELD_REQUIREDFITNESS, requiredFitness);

		return root;
	}

	@Override
	public boolean isFinished()
	{
		BrainPerformance bestPerformance = getResultPerformance();

		if(bestPerformance == null)
			return false;

		double bestFitness = bestPerformance.getFitness();

		return requiredFitness == null || bestFitness >= requiredFitness;
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(o == null || getClass() != o.getClass())
			return false;
		if(!super.equals(o))
			return false;

		Training training = (Training) o;

		if(requiredFitness != null ? !requiredFitness.equals(training.requiredFitness) : training.requiredFitness != null)
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = super.hashCode();
		result = 31 * result + (requiredFitness != null ? requiredFitness.hashCode() : 0);
		return result;
	}
}
