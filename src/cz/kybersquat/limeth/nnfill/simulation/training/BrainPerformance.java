package cz.kybersquat.limeth.nnfill.simulation.training;

import cz.kybersquat.limeth.nnfill.world.CreatureBrain;

/**
 * @author Limeth
 */
public class BrainPerformance implements Comparable<BrainPerformance>
{
	private CreatureBrain brain;
	private double fitness;

	public BrainPerformance(CreatureBrain brain, double fitness)
	{
		if(brain == null)
			throw new IllegalArgumentException("The brain must not be null!");

		this.brain = brain;
		this.fitness = fitness;
	}

	public CreatureBrain getBrain()
	{
		return brain;
	}

	public double getFitness()
	{
		return fitness;
	}

	@Override
	public int compareTo(BrainPerformance o)
	{
		double delta = fitness - o.fitness;

		return delta < 0 ? -1 : (delta > 0 ? 1 : 0);
	}

	@Override
	public String toString()
	{
		return "BrainPerformance{" +
		       "brain=" + brain +
		       ", fitness=" + fitness +
		       '}';
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(o == null || getClass() != o.getClass())
			return false;

		BrainPerformance that = (BrainPerformance) o;

		if(Double.compare(that.fitness, fitness) != 0)
			return false;
		if(!brain.equals(that.brain))
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result;
		long temp;
		result = brain.hashCode();
		temp = Double.doubleToLongBits(fitness);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
}
