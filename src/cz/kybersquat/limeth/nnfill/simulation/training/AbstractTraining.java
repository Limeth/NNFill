package cz.kybersquat.limeth.nnfill.simulation.training;

import com.google.common.collect.Sets;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import cz.kybersquat.limeth.nnfill.json.Jsonable;
import cz.kybersquat.limeth.nnfill.simulation.FilterLayer;
import cz.kybersquat.limeth.nnfill.simulation.Simulation;
import cz.kybersquat.limeth.nnfill.world.CreatureBrain;
import neuroNet.limeth.network.NeuralNetwork;

import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * @author Limeth
 */
public abstract class AbstractTraining implements Jsonable
{
	private static final String JSON_FIELD_NAME = "name";
	private static final String JSON_FIELD_ORIGINALBRAIN = "originalBrain";
	private static final String JSON_FIELD_RESULTPERFORMANCE = "resultPerformance";
	private static final String JSON_FIELD_SIMULATION = "simulation";
	private static final String JSON_FIELD_THREADAMOUNT = "threadAmount";
	private String                         name;
	private CreatureBrain                  originalBrain;
	private BrainPerformance               resultPerformance;
	private Simulation             simulation;
	//private TreeMultiset<BrainPerformance> performances;
	private Set<TrainingThread>                    threads;
	private UpgradeHandler upgradeHandler;
	private final Object lock = new Object();

	public AbstractTraining(String name, NeuralNetwork originalNetwork, Simulation simulation, int threadAmount)
	{
		this.name = name == null ? "default" : name;
		this.originalBrain = CreatureBrain.of(originalNetwork);
		this.simulation = simulation;
		//this.performances = TreeMultiset.create();
		threads = Sets.newHashSet();

		for(int i = 0; i < threadAmount; i++)
			threads.add(new TrainingThread(name + " " + (i + 1), this, simulation.clone(), originalBrain.clone()));
	}

	public AbstractTraining(JsonElement rawRoot, JsonDeserializationContext context)
	{
		JsonObject root = rawRoot.getAsJsonObject();
		this.name = root.has(JSON_FIELD_NAME) && !root.get(JSON_FIELD_NAME).isJsonNull()
		            ? root.get(JSON_FIELD_NAME).getAsString() : "default";
		this.originalBrain = context.deserialize(root.get(JSON_FIELD_ORIGINALBRAIN), CreatureBrain.class);
		simulation = context.deserialize(root.get(JSON_FIELD_SIMULATION), Simulation.class);
		int threadAmount = root.get(JSON_FIELD_THREADAMOUNT).getAsInt();
		//this.performances = TreeMultiset.create();
		threads = Sets.newHashSet();

		for(int i = 0; i < threadAmount; i++)
			threads.add(new TrainingThread(name + " " + (i + 1), this, simulation.clone(), originalBrain.clone()));
	}

	public abstract boolean isFinished();

	@Override
	public JsonObject toJson(JsonSerializationContext context)
	{
		JsonObject root = new JsonObject();

		root.addProperty(JSON_FIELD_NAME, name);
		root.addProperty(JSON_FIELD_THREADAMOUNT, threads.size());
		root.add(JSON_FIELD_ORIGINALBRAIN, context.serialize(originalBrain));
		root.add(JSON_FIELD_RESULTPERFORMANCE, context.serialize(resultPerformance));
		root.add(JSON_FIELD_SIMULATION, context.serialize(simulation));

		return root;
	}

	public AbstractTraining begin()
	{
		List<BrainPerformance> currentPerformances = getOriginalSimulation().clone().filter(originalBrain);

		Collections.sort(currentPerformances);

		resultPerformance = currentPerformances.get(currentPerformances.size() - 1);

		threads.forEach(Thread::start);

		for(TrainingThread thread : threads)
			try
			{
				thread.join();
			}
			catch(InterruptedException e)
			{
				e.printStackTrace();
			}

		return this;
	}

	public void onUpgrade()
	{
		if(upgradeHandler != null)
			upgradeHandler.onUpgrade(this);
	}

	public void setUpgradeHandler(UpgradeHandler upgradeHandler)
	{
		this.upgradeHandler = upgradeHandler;
	}

	public UpgradeHandler getUpgradeHandler()
	{
		return upgradeHandler;
	}

	public Object getLock()
	{
		return lock;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name == null ? "default" : null;
	}

	void setResultPerformanceUnlocked(BrainPerformance resultPerformance)
	{
		this.resultPerformance = resultPerformance;
	}

	BrainPerformance getResultPerformanceUnlocked()
	{
		return resultPerformance;
	}

	public BrainPerformance getResultPerformance()
	{
		synchronized(lock)
		{
			return resultPerformance;
		}
	}

	public CreatureBrain getOriginalBrain()
	{
		return originalBrain;
	}

	public void setOriginalBrain(CreatureBrain originalBrain)
	{
		this.originalBrain = originalBrain;
	}

/*	public TreeMultiset<BrainPerformance> getPerformances()
	{
		return performances;
	}

	public void setPerformances(TreeMultiset<BrainPerformance> performances)
	{
		this.performances = performances;
	}*/

	public Simulation getSimulation()
	{
		return simulation;
	}

	public Simulation getOriginalSimulation()
	{
		return simulation instanceof FilterLayer ? ((FilterLayer) simulation).getSimulation() : simulation;
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(o == null || getClass() != o.getClass())
			return false;

		AbstractTraining training = (AbstractTraining) o;

		if(name != null ? !name.equals(training.name) : training.name != null)
			return false;
		if(!originalBrain.equals(training.originalBrain))
			return false;
		if(!simulation.equals(training.simulation))
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = name != null ? name.hashCode() : 0;
		result = 31 * result + originalBrain.hashCode();
		result = 31 * result + simulation.hashCode();
		return result;
	}
}
