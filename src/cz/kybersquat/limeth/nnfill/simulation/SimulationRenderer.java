package cz.kybersquat.limeth.nnfill.simulation;

/**
 * @author Limeth
 */
public interface SimulationRenderer
{
	void render(AbstractSimulation simulation);
}
