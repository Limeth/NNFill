package cz.kybersquat.limeth.nnfill.simulation;

import cz.kybersquat.limeth.nnfill.Vector2i;
import cz.kybersquat.limeth.nnfill.simulation.training.BrainPerformance;
import cz.kybersquat.limeth.nnfill.world.*;
import cz.kybersquat.limeth.nnfill.world.shape.WorldShape;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Limeth
 */
public abstract class AbstractSimulation implements Simulation
{
	private World              world;

	public AbstractSimulation(World world)
	{
		this.world = world;
	}

	public AbstractSimulation(WorldShape worldShape)
	{
		this(new World(worldShape));
	}

	public abstract Vector2i getCreatureOrigin();
	public abstract SimulationRenderer getRenderer();
	public abstract void setRenderer(SimulationRenderer renderer);
	public abstract AbstractSimulation clone();

	@Override
	public List<BrainPerformance> filter(CreatureBrain brain)
	{
		World world = getWorld();

		world.clear();

		Creature creature = createCreature(brain);

		render();

		while(world.getShape().isInside(creature.getLocation()) && !world.isOccupied(creature.getLocation()))
			update();

		BrainPerformance performance = new BrainPerformance(brain, creature.getProgress());
		List<BrainPerformance> performances = new ArrayList<>();

		performances.add(performance);

		return performances;
	}

	private Creature createCreature(CreatureBrain brain, Vector2i location)
	{
		World world = getWorld();
		Creature creature = new Creature(world, location, Face.NORTH, brain).attachToBrain();
		List<Entity> entities = world.getEntities();

		entities.add(creature);

		return creature;
	}

	private Creature createCreature(CreatureBrain brain)
	{
		return createCreature(brain, getCreatureOrigin());
	}

	public void update()
	{
		World world = getWorld();

		world.update();
		render();
	}

	public void render()
	{
		SimulationRenderer renderer = getRenderer();

		if(renderer != null)
			renderer.render(this);
	}

/*	public double getResultFitness() throws IllegalStateException
	{
		if(!isFinished())
			throw new IllegalStateException("The simulation has not finished.");

		return creature.getProgress();
	}*/

/*	@Override
	public boolean isValid()
	{
		return worldShape.isInside(creature.getLocation()) && !world.isOccupied(creature.getLocation());
	}*/

/*	public NeuralNetwork getOriginalNetwork()
	{
		return originalNetwork;
	}*/

	@Override
	public World getWorld()
	{
		return world;
	}

/*	@Override
	public void setOriginalBrain(CreatureBrain originalBrain)
	{
		this.originalBrain = originalBrain;

		if(creature != null)
			this.originalBrain.attachCreature(creature);
	}*/

/*	public CreatureBrain getResultBrain()
	{
		return originalBrain;
	}*/

/*	@Override
	public BrainPerformance getResultPerformance() throws IllegalStateException
	{
		return new BrainPerformance(getResultBrain(), getResultFitness());
	}*/
}
