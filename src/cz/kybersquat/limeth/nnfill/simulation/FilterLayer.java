package cz.kybersquat.limeth.nnfill.simulation;

/**
 * @author Limeth
 */
public interface FilterLayer extends Simulation
{
	void setChild(Simulation simulation);
	Simulation getChild();
	void setSimulation(Simulation simulation);
	Simulation getSimulation();
	FilterLayer clone();
}
