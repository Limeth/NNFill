package cz.kybersquat.limeth.nnfill.simulation;

import cz.kybersquat.limeth.nnfill.json.Jsonable;
import cz.kybersquat.limeth.nnfill.simulation.training.BrainPerformance;
import cz.kybersquat.limeth.nnfill.world.CreatureBrain;
import cz.kybersquat.limeth.nnfill.world.World;

import java.util.List;

/**
 * @author Limeth
 */
public interface Simulation extends Cloneable, Jsonable
{
	List<BrainPerformance> filter(CreatureBrain brain);
	World getWorld();
	void setRenderer(SimulationRenderer renderer);
	SimulationRenderer getRenderer();
	Simulation clone();
}
