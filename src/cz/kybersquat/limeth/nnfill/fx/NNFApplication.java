package cz.kybersquat.limeth.nnfill.fx;

import cz.kybersquat.limeth.nnfill.simulation.AbstractSimulation;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import neuroNet.limeth.network.NeuralNetwork;

import java.io.File;

public class NNFApplication extends Application
{
	private static AbstractSimulation simulation;
	private static NeuralNetwork      network;
	private static File               outputFile;

	@Override
	public void start(Stage primaryStage) throws Exception
	{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("layout.fxml"));

		loader.setController(new NNFController(this));

		Parent parent = loader.load();

        primaryStage.setScene(new Scene(parent));
		primaryStage.setTitle("NNFill Simulation");
        primaryStage.show();
    }

	public static AbstractSimulation getSimulation()
	{
		return simulation;
	}

	public static void setSimulation(AbstractSimulation simulation)
	{
		NNFApplication.simulation = simulation;
	}

	public static NeuralNetwork getNetwork()
	{
		return network;
	}

	public static void setNetwork(NeuralNetwork network)
	{
		NNFApplication.network = network;
	}

	public static File getOutputFile()
	{
		return outputFile;
	}

	public static void setOutputFile(File outputFile)
	{
		NNFApplication.outputFile = outputFile;
	}
}
