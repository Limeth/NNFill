package cz.kybersquat.limeth.nnfill.fx;

import cz.kybersquat.limeth.nnfill.Vector2i;
import cz.kybersquat.limeth.nnfill.simulation.AbstractSimulation;
import cz.kybersquat.limeth.nnfill.world.CreatureBrain;
import cz.kybersquat.limeth.nnfill.world.Entity;
import cz.kybersquat.limeth.nnfill.world.World;
import cz.kybersquat.limeth.nnfill.world.shape.WorldShape;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import neuroNet.limeth.network.NeuralNetwork;

import java.util.List;

/**
 * @author Limeth
 */
public class NNFController implements EventHandler<ActionEvent>
{
	private static final int PERIOD_STEP = 1_000;
	private NNFApplication application;
	@FXML
	private Button         stepButton;
	@FXML
	private Button         autoButton;
	@FXML
	private GridPane       gridPane;
	private final Object lock = new Object();
	private Integer stepPeriod;

	public NNFController(NNFApplication application)
	{
		this.application = application;
	}

	public void initialize()
	{
		AbstractSimulation simulation = NNFApplication.getSimulation();
		NeuralNetwork network = NNFApplication.getNetwork();
		World world = simulation.getWorld();
		WorldShape worldShape = world.getShape();
		int worldWidth = worldShape.getWidth();
		int worldHeight = worldShape.getHeight();

		for(int j = 0; j < worldWidth; j++)
		{
			ColumnConstraints cc = new ColumnConstraints();
			cc.setHgrow(Priority.ALWAYS);
			gridPane.getColumnConstraints().add(cc);
		}

		for(int j = 0; j < worldHeight; j++)
		{
			RowConstraints rc = new RowConstraints();
			rc.setVgrow(Priority.ALWAYS);
			rc.setFillHeight(true);
			gridPane.getRowConstraints().add(rc);
		}

		simulation.setRenderer(this::render);
		stepButton.setOnAction(this);
		autoButton.setOnAction(this);
		run(simulation, network);
	}

	private Thread run(AbstractSimulation simulation, NeuralNetwork network)
	{
		CreatureBrain brain = CreatureBrain.of(network);
		Thread thread = new Thread(() -> {
			simulation.filter(brain);

			Platform.runLater(() -> {
				stepButton.setDisable(true);
				autoButton.setDisable(true);
			});
		});

		thread.start();

		return thread;
	}

	@Override
	public void handle(ActionEvent event)
	{
		Object source = event.getSource();

		if(source == stepButton)
			stepPress();
		else if(source == autoButton)
			autoPress();
	}

	private void stepPress()
	{
		synchronized(lock)
		{
			stepPeriod = null;
			lock.notifyAll();
		}
	}

	private void autoPress()
	{
		synchronized(lock)
		{
			stepPeriod = PERIOD_STEP;
			lock.notifyAll();
		}
	}

	private void render(AbstractSimulation simulation)
	{
		Platform.runLater(() -> render(simulation.getWorld()));

		synchronized(lock)
		{
			try
			{
				if(stepPeriod != null)
					lock.wait(stepPeriod);
				else
					lock.wait();
			}
			catch(InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}

	public void render(World world)
	{
		List<Entity> entities = world.getEntities();
		WorldShape worldShape = world.getShape();
		int worldWidth = worldShape.getWidth();
		int worldHeight = worldShape.getHeight();

		gridPane.getChildren().clear();

		for(int y = 0; y < worldHeight; y++)
			for(int x = 0; x < worldWidth; x++)
			{
				int gridX = x;
				int gridY = worldHeight - y - 1;
				Paint color = worldShape.isInside(x, y) ? (!world.isOccupied(x, y) ? Color.WHITE : Color.BLUE) : Color.BLACK;

				cell(color, gridX, gridY, worldWidth, worldHeight);
			}

		for(Entity entity : entities)
		{
			Vector2i location = entity.getLocation();
			int x = location.getX();
			int y = location.getY();

			if(x < 0 || y < 0 || x >= worldWidth || y >= worldHeight)
				continue;

			int gridX = x;
			int gridY = worldHeight - y - 1;

			cell(entity.getColor(), gridX, gridY, worldWidth, worldHeight);
		}
	}

	private Node cell(Paint paint, int x, int y, int columns, int rows)
	{
		Rectangle rectangle = new Rectangle(1, 1, paint);

		gridPane.add(rectangle, x, y);
		rectangle.widthProperty().bind(gridPane.widthProperty().subtract(gridPane.getHgap() * (columns - 1)).divide(columns));
		rectangle.heightProperty().bind(gridPane.heightProperty().subtract(gridPane.getVgap() * (columns - 1)).divide(rows));

		return rectangle;
	}
}
