package cz.kybersquat.limeth.nnfill.world.shape;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import cz.kybersquat.limeth.nnfill.json.NNFillAdapterFactory;
import junit.framework.Assert;
import org.junit.Test;

import java.util.BitSet;

public class BitmapShapeTest
{
	@Test
	public void testJson()
	{
		int size = 8;
		BitSet bitset = new BitSet(size * size);

		for(int y = 0; y < size; y++)
			for(int x = y == 0 ? 1 : 0; x < size; x += 2)
				bitset.set(x + y * size);

		BitmapShape shape = new BitmapShape(bitset, size, size);
		Gson gson = NNFillAdapterFactory.createBuilder().setPrettyPrinting().create();
		JsonElement json = gson.toJsonTree(shape);
		WorldShape deserializedShape = gson.fromJson(json, WorldShape.class);

		Assert.assertEquals(shape, deserializedShape);
	}
}