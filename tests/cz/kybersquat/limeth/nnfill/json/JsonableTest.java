package cz.kybersquat.limeth.nnfill.json;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import junit.framework.Assert;
import org.junit.Test;

public class JsonableTest
{
	@Test
	public void testJsonable() throws Exception
	{
		Gson gson = NNFillAdapterFactory.createBuilder().create();
		JsonableImpl original = new JsonableImpl(10);
		JsonElement json = gson.toJsonTree(original);
		JsonableImpl deserialized = gson.fromJson(json, JsonableImpl.class);

		Assert.assertEquals(original, deserialized);
	}
}

class JsonableImpl implements Jsonable
{
	private int number;

	public JsonableImpl(int number)
	{
		this.number = number;
	}

	public static JsonableImpl fromJson(JsonElement root, JsonDeserializationContext context)
	{
		return new JsonableImpl(root.getAsInt());
	}

	@Override
	public JsonElement toJson(JsonSerializationContext context)
	{
		return context.serialize(number);
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(o == null || getClass() != o.getClass())
			return false;

		JsonableImpl jsonable = (JsonableImpl) o;

		if(number != jsonable.number)
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		return number;
	}
}