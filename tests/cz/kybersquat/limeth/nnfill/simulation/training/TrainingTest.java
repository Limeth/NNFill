package cz.kybersquat.limeth.nnfill.simulation.training;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import cz.kybersquat.limeth.nnfill.Vector2i;
import cz.kybersquat.limeth.nnfill.json.NNFillAdapterFactory;
import cz.kybersquat.limeth.nnfill.simulation.AbstractSimulation;
import cz.kybersquat.limeth.nnfill.simulation.FilterLayer;
import cz.kybersquat.limeth.nnfill.simulation.Simulation;
import cz.kybersquat.limeth.nnfill.simulation.training.layers.ExpansionFilterLayer;
import cz.kybersquat.limeth.nnfill.simulation.training.layers.MutationFilterLayer;
import cz.kybersquat.limeth.nnfill.simulation.training.layers.TrainingSimulation;
import cz.kybersquat.limeth.nnfill.world.CreatureBrain;
import cz.kybersquat.limeth.nnfill.world.Face;
import cz.kybersquat.limeth.nnfill.world.shape.UnitedShape;
import junit.framework.Assert;
import neuroNet.limeth.network.NeuralNetwork;
import neuroNet.limeth.network.functions.ActivationFunction;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;

public class TrainingTest
{
	private Training training;

	@Before
	public void setUp()
	{
		AbstractSimulation originalFilterLayer = createSimulation();
		training = createTraining("Training", originalFilterLayer);
	}

	@Test
	public void testJson()
	{
		Gson gson = NNFillAdapterFactory.createBuilder().setPrettyPrinting().create();
		JsonElement json = gson.toJsonTree(training);
		AbstractTraining deserialized = gson.fromJson(json, AbstractTraining.class);

		Assert.assertEquals(training, deserialized);
	}

	private static NeuralNetwork createNetwork()
	{
		ActivationFunction activationFunction = ActivationFunction.LINEAR;
		int[] hiddenLayers = new int[]{20, 20};
		return CreatureBrain.createNetwork(activationFunction, 0, hiddenLayers, true);
	}

	private static AbstractSimulation createSimulation()
	{
		float radius = 10;

		UnitedShape shape = UnitedShape.builder().rectPos(0, 1, 0, 7).rectPos(0, 7, 7, 7).rectPos(7, 7, 7, 0)
		                               .rectPos(7, 0, 1, 0)
		                               .build();

		return new TrainingSimulation(shape, new Vector2i(0, 1), Face.NORTH);
	}

	private static FilterLayer createFilters(Simulation simulation, int[] levels)
	{
		double mutationChance = 1;
		double mutationWeight = 1;

		FilterLayer filterLayer = new MutationFilterLayer(simulation, new Random(), mutationChance, mutationWeight);

		for(int level : levels)
			filterLayer = new ExpansionFilterLayer(filterLayer, level);

		//filterLayer = new LimitingFilterLayer(filterLayer, 1);

		return filterLayer;
	}

	private static Training createTraining(String name, Simulation simulation)
	{
		NeuralNetwork originalNetwork = createNetwork();
		FilterLayer filterLayer = createFilters(simulation, new int[0]);

		return new Training(name, originalNetwork, filterLayer, 8, 0.9);
	}
}